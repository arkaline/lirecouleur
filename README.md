# lirecouleur

LireCouleur est un ensemble d'outils pour aider les personnes dyslexiques et pour les débutants lecteurs.
Le projet se décline en deux applications :
* une extension pour LibreOffice ou Apache OpenOffice qui permet de présenter du texte selon différents modes afin d'être plus facile à lire
* une application Web qui permet de coder un texte sans avoir à installer d'outil supplémentaire
