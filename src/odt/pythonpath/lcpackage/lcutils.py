#!/usr/bin/env python
# -*- coding: UTF-8 -*-

###################################################################################
# LireCouleur - outils d'aide à la lecture
#
# voir http://lirecouleur.arkaline.fr
#
# @author Marie-Pierre Brungard
# @version 5.3.0
# @since 2022
# https://forum.openoffice.org/en/forum/viewtopic.php?f=20&t=87057
#
# GNU General Public Licence (GPL) version 3
#
# LireCouleur is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.
# LireCouleur is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
# You should have received a copy of the GNU General Public License along with
# LireCouleur; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA  02111-1307  USA
###################################################################################
import uno
import unohelper
import os
import json
import inspect
import codecs

from com.sun.star.awt.PosSize import POSSIZE as PS_POSSIZE

CONFIG_NODE = "/lire.libre.lirecouleur/Settings"
CURRENT_DIRECTORY = os.path.dirname(os.path.abspath(inspect.getframeinfo(inspect.currentframe()).filename))

"""
    Constantes LireCouleur
"""
class ConstLireCouleur:
    # différentes configurations de marquage des syllabes
    SYLLABES_LC = 0
    SYLLABES_STD = 1
    SYLLABES_ORALES = 1
    SYLLABES_ECRITES = 0

    # prononciation différente entre l'Europe et le Canada
    MESTESSESLESDESCES = {'':'e_comp','fr':'e_comp','fr_CA':'e^_comp'}

"""
    Fonctions utilitaires
"""
from com.sun.star.awt.MessageBoxButtons import BUTTONS_OK
from com.sun.star.awt.MessageBoxType import ERRORBOX
def LireCouleurLogBox(message="", box_type=ERRORBOX, buttons=BUTTONS_OK, title="LireCouleurLogBox"):
    return
    
    """ctx = uno.getComponentContext()
    
    try:    
        # for debug only
        resolver = ctx.ServiceManager.createInstanceWithContext("com.sun.star.bridge.UnoUrlResolver", ctx)
        ctx = resolver.resolve("uno:socket,host=localhost,port=2002;urp;StarOffice.ComponentContext")
        # end for debug only
    except:
        pass

    smgr = ctx.ServiceManager
    desktop = smgr.createInstanceWithContext( "com.sun.star.frame.Desktop",ctx)
    frame = desktop.getCurrentFrame()
    window = frame.getContainerWindow()
    toolkit = window.getToolkit()
        
    messagebox = toolkit.createMessageBox(window, box_type, buttons, title, message)
    return messagebox.execute()"""

def create_uno_struct(cTypeName):
    """Create a UNO struct and return it.
    Similar to the function of the same name in OOo Basic. -- Copied from Danny Brewer library
    """
    try:
        sm = uno.getComponentContext().ServiceManager
        oCoreReflection = sm.createInstance( "com.sun.star.reflection.CoreReflection" )
        # Get the IDL class for the type name
        oXIdlClass = oCoreReflection.forName( cTypeName )
        # Create the struct.
        __oReturnValue, oStruct = oXIdlClass.createObject( None )
        return oStruct
    except:
        LireCouleurLogBox('erreur create_uno_struct - '+cTypeName)
        return None

def create_uno_service(serviceName, ctx=None):
    if ctx is None:
        ctx = uno.getComponentContext()

    sm = ctx.ServiceManager
    try:
        return sm.createInstanceWithContext(serviceName, ctx)
    except:
        pass

    try:
        return sm.createInstance(serviceName)
    except:
        pass
    
    LireCouleurLogBox('erreur create_uno_service - '+serviceName)
    return None

def create_control(ctx, control_type, x, y, width, height, names, values):
    """ create a control. """
    smgr = ctx.getServiceManager()
    
    ctrl = smgr.createInstanceWithContext(
        "com.sun.star.awt." + control_type, ctx)
    ctrl_model = smgr.createInstanceWithContext(
        "com.sun.star.awt." + control_type + "Model", ctx)
    
    if len(names) > 0:
        ctrl_model.setPropertyValues(names, values)
    ctrl.setModel(ctrl_model)
    ctrl.setPosSize(x, y, width, height, PS_POSSIZE)
    return ctrl


def create_container(ctx, parent, names, values, __fit=True):
    """ create control container. """
    cont = create_control(ctx, "UnoControlContainer", 0, 0, 0, 0, names, values)
    cont.createPeer(parent.getToolkit(), parent)
    #if fit:
    #    cont.setPosSize()
    return cont


def create_controls(ctx, container, controls):
    """
    ((TYPE, NAME, x, y, width, height, PROP_NAMES, PROP_VALUES, OPTIONS), ())
    """
    smgr = ctx.getServiceManager()
    for defs in controls:
        c = smgr.createInstanceWithContext(
            "com.sun.star.awt.UnoControl" + defs[0], ctx)
        cm = smgr.createInstanceWithContext(
            "com.sun.star.awt.UnoControl" + defs[0] + "Model", ctx)
        cm.setPropertyValues(defs[6], defs[7])
        c.setModel(cm)
        c.setPosSize(defs[2], defs[3], defs[4], defs[5], PS_POSSIZE)
        
        container.addControl(defs[1], c)
        if len(defs) == 9:
            options = defs[8]
            if defs[0] == "Button":
                if "ActionCommand" in options:
                    c.setActionCommand(options["ActionCommand"])
                if "ActionListener" in options:
                    c.addActionListener(options["ActionListener"])
            elif defs[0] == "Combo":
                if "TextListener" in options:
                    c.addTextListener(options["TextListener"])
        
    return container

def get_backgroundcolor(window):
    """ Get background color through accesibility api. """
    try:
        return window.getAccessibleContext().getBackground()
    except:
        pass
    return 0xeeeeee


"""from com.sun.star.task import XInteractionHandler
class DummyHandler(unohelper.Base, XInteractionHandler):
    # dummy XInteractionHanlder interface for the StringResouceWithLocation
    def __init__(self): pass
    def handle(self,request): pass"""


from com.sun.star.lang import Locale
from com.sun.star.beans import PropertyValue
from com.sun.star.beans.PropertyState import DIRECT_VALUE as PS_DIRECT_VALUE


def get_config_access(ctx, nodepath, updatable=False):
    """ get configuration access. """
    try:
        arg = PropertyValue("nodepath", 0, nodepath, PS_DIRECT_VALUE)
        cp = ctx.getServiceManager().createInstanceWithContext(
            "com.sun.star.configuration.ConfigurationProvider", ctx)
        if updatable:
            return cp.createInstanceWithArguments(
            "com.sun.star.configuration.ConfigurationUpdateAccess", (arg,))
        else:
            return cp.createInstanceWithArguments(
            "com.sun.star.configuration.ConfigurationAccess", (arg,))
    except:
        LireCouleurLogBox('erreur get_config_access - '+nodepath)
        return None


def get_config_value(ctx, nodepath, name):
    """ get configuration value. """
    cua = get_config_access(ctx, nodepath)
    if not cua is None:
        return cua.getPropertyValue(name)
    return ""


def get_ui_locale(ctx):
    """ get UI locale as css.lang.Locale struct. """
    loc = get_config_value(ctx, "/org.openoffice.Setup/L10N", "ooLocale")
    if "-" in loc:
        parts = loc.split("-")
        locale = Locale(parts[0], parts[1], "")
    else:
        locale = Locale(loc, "", "")
    return locale


def create_dialog(ctx, url):
    """ create dialog from url. """
    try:
        return ctx.getServiceManager().createInstanceWithContext(
            "com.sun.star.awt.DialogProvider", ctx).createDialog(url)
    except:
        LireCouleurLogBox('erreur create_dialog')
        return None

"""
    Get the URL of LireCouleur
"""
def getLirecouleurURL():
    """Get the URL of LireCouleur"""
    try:
        pip = uno.getComponentContext().getValueByName("/singletons/com.sun.star.deployment.PackageInformationProvider")
        url = pip.getPackageLocation("lire.libre.lirecouleur")
        if len(url) > 0:
            return url
    except:
        pass

    try:
        # just for debugging outside the extension scope
        filename = uno.fileUrlToSystemPath(__file__)
        return uno.systemPathToFileUrl(os.path.dirname(os.path.abspath(filename)))
    except:
        pass

    return uno.systemPathToFileUrl(CURRENT_DIRECTORY)

"""
    Load and set configuration values.
"""
class Settings(object):
    _loaded = False
    FPossibles = None
    TaillIco = 32
    Fonctions = {}
    SelectionPhonemes = {}
    SelectionLettres = {}
    SelectionFonctions = []
    Template = ""
    Simple = False
    Point = False
    Syllo = (ConstLireCouleur.SYLLABES_LC, ConstLireCouleur.SYLLABES_ECRITES)
    Alternate = 2
    Locale = 'fr'
    Threads = True
    LProfils = []

    """ Load and set configuration values. """
    def __init__(self, ctx=None):
        try:
            self.ctx = ctx
            if self.ctx is None:
                self.ctx = uno.getComponentContext()
        except:
            pass
    
    def getPropertyValue(self, cua, name):
        try:
            return cua.getPropertyValue(name)
        except:
            pass
        try:
            return self._settings[name]
        except:
            pass
        return ""
    
    def _load(self):
        cua = get_config_access(self.ctx, CONFIG_NODE)
        if cua is None:
            f = codecs.open(os.sep.join([CURRENT_DIRECTORY, "settings.json"]), "r", "utf_8_sig", errors="replace")
            self._settings = json.load(f)
            f.close()

        profilsUrl = os.sep.join([getLirecouleurURL(), "template"])
        
        Settings.FPossibles = self.getPropertyValue(cua, "__fonctions_possibles__").split(':')
        Settings.TaillIco = self.getPropertyValue(cua, "__taille_icones__")
        Settings.Fonctions = {}
        for fct in Settings.FPossibles:
            prop = self.getPropertyValue(cua, fct)
            if not prop is None:
                Settings.Fonctions[fct] = prop.split(':')
                Settings.Fonctions[fct][2] = eval(self.Fonctions[fct][2])
                if Settings.Fonctions[fct][2]:
                    Settings.SelectionFonctions.append(fct)
        
        prop = self.getPropertyValue(cua, "__selection_phonemes__")
        if not prop is None:
            selphon = [ph.split(':') for ph in prop.split(';')]
            Settings.SelectionPhonemes = dict([[ph[0], eval(ph[1])] for ph in selphon])
            # considérer que la sélection des phonèmes 'voyelle' s'étend à 'yod'+'voyelle' et à 'wau'+'voyelle'
            for phon in ['a', 'a~', 'e', 'e^', 'e_comp', 'e^_comp', 'o','o_comp', 'o~', 'i', 'e~', 'x', 'x^', 'u', 'q_caduc']:
                try:
                    Settings.SelectionPhonemes['j_'+phon] = Settings.SelectionPhonemes[phon]
                    Settings.SelectionPhonemes['w_'+phon] = Settings.SelectionPhonemes[phon]
                except:
                    Settings.SelectionPhonemes[phon] = Settings.SelectionPhonemes['j_'+phon] = Settings.SelectionPhonemes['w_'+phon] = 0
        prop = self.getPropertyValue(cua, "__selection_lettres__")
        if not prop is None:
            selphon = [ph.split(':') for ph in prop.split(';')]
            Settings.SelectionLettres = dict([[ph[0], eval(ph[1])] for ph in selphon])
        Settings.Template = self.getPropertyValue(cua, "__template__")
        try:
            if not os.path.isfile(uno.fileUrlToSystemPath(Settings.Template)):
                # cas 1 : le nom du modèle serait le nom d'un profil stocké dans "template"
                Settings.Template = os.sep.join([profilsUrl, Settings.Template])
        except:
            pass
        try:
            if not os.path.isfile(uno.fileUrlToSystemPath(Settings.Template)):
                # cas 2 : le nom du modèle ne correspond à rien - on supprime
                Settings.Template = ""
        except:
            Settings.Template = ""

        Settings.Simple = self.getPropertyValue(cua, "__detection_phonemes__")
        Settings.Point = self.getPropertyValue(cua, "__point__")
        choix_syllo = self.getPropertyValue(cua, "__syllo__")
        if not isinstance(choix_syllo, int):
            Settings.Syllo = (ConstLireCouleur.SYLLABES_LC, ConstLireCouleur.SYLLABES_ECRITES)
        else:
            Settings.Syllo = (choix_syllo%2, int(choix_syllo/10)%2)
        if self.Syllo[1]:
            Settings.SelectionPhonemes['q_caduc'] = Settings.SelectionPhonemes['yod_q_caduc'] = Settings.SelectionPhonemes['#']
        else:
            Settings.SelectionPhonemes['q_caduc'] = Settings.SelectionPhonemes['yod_q_caduc'] = Settings.SelectionPhonemes['q']

        Settings.Alternate = self.getPropertyValue(cua, "__alternate__")
        Settings.Locale = self.getPropertyValue(cua, "__locale__")
        
        # lecture des profils et suppression des profils dont les fichiers ne correspondent plus à des fichiers
        Settings.LProfils = self.getPropertyValue(cua, "__profils__").split(';')
        lsupr = []
        for i in range(len(Settings.LProfils)):
            try:
                if not os.path.isfile(uno.fileUrlToSystemPath(Settings.LProfils[i])):
                    # cas 1 : le nom du modèle serait le nom d'un profil stocké dans "template"
                    xtmp = os.sep.join([profilsUrl, Settings.LProfils[i]])
                    if os.path.isfile(uno.fileUrlToSystemPath(xtmp)):
                        # cas 2 : le nom du modèle ne correspond à rien - on supprime
                        Settings.LProfils[i] = xtmp
                    else:
                        lsupr.append(i)
            except:
                lsupr.append(i)
        lsupr.reverse()
        for i in lsupr:
            del Settings.LProfils[i]
        del lsupr

        Settings.Threads = self.getPropertyValue(cua, "__thread__")

        Settings._loaded = True
    
    def get(self, name):
        """ get specified value. """
        if not Settings._loaded:
            self._load()
        if name == "__fonctions_possibles__":
            return Settings.FPossibles
        if name == "__selection_fonctions__":
            return Settings.SelectionFonctions
        if name == "__taille_icones__":
            return Settings.TaillIco
        if name == "__selection_phonemes__":
            return Settings.SelectionPhonemes
        if name == "__selection_lettres__":
            return Settings.SelectionLettres
        if name == "__template__":
            return Settings.Template
        if name == "__point__":
            return Settings.Point
        if name == "__detection_phonemes__":
            return Settings.Simple
        if name == "__syllo__":
            return Settings.Syllo
        if name == "__alternate__":
            return Settings.Alternate
        if name == "__locale__":
            return Settings.Locale
        if name == "__thread__":
            return Settings.Threads
        if name == "__profils__":
            return Settings.LProfils
        try:
            return Settings.Fonctions[name]
        except:
            LireCouleurLogBox('erreur Settings.get '+name)
            return ""
        return ""

    def getStrValue(self, name):
        """ get specified value. """
        if not Settings._loaded:
            self._load()
        if name == "__taille_icones__":
            return str(Settings.TaillIco)
        elif name == "__fonctions_possibles__":
            return ':'.join(Settings.FPossibles)
        elif name == "__selection_fonctions__":
            return ':'.join(Settings.SelectionFonctions)
        elif name == "__selection_phonemes__":
            return ';'.join([ph+':'+str(Settings.SelectionPhonemes[ph]) for ph in Settings.SelectionPhonemes.keys()])
        elif name == "__selection_lettres__":
            return ';'.join([ph+':'+str(Settings.SelectionLettres[ph]) for ph in Settings.SelectionLettres.keys()])
        elif name == "__template__":
            return str(Settings.Template)
        elif name == "__point__":
            return str(Settings.Point)
        elif name == "__thread__":
            return str(Settings.Threads)
        elif name == "__detection_phonemes__":
            return str(Settings.Simple)
        elif name == "__syllo__":
            return str(Settings.Syllo[1]*10+Settings.Syllo[0])
        elif name == "__alternate__":
            return str(Settings.Alternate)
        elif name == "__locale__":
            return Settings.Locale
        elif name == "__profils__":
            return ';'.join(prof for prof in Settings.LProfils)
        return ""

    def setStrValue(self, name, value):
        if not Settings._loaded:
            self._load()
        """ set specified value. """
        if name == "__taille_icones__":
            Settings.TaillIco = int(value)
            self.setValue(name, Settings.TaillIco)
        elif name == "__selection_phonemes__":
            prop = value
            if not prop is None:
                selphon = [ph.split(':') for ph in prop.split(';')]
                Settings.SelectionPhonemes = dict([[ph[0], eval(ph[1])] for ph in selphon])
                # considérer que la sélection des phonèmes 'voyelle' s'étend à 'yod'+'voyelle' et à 'wau'+'voyelle'
                for phon in ['a', 'a~', 'e', 'e^', 'e_comp', 'e^_comp', 'o', 'o~', 'i', 'e~', 'x', 'x^', 'u', 'q_caduc']:
                    try:
                        Settings.SelectionPhonemes['j_'+phon] = Settings.SelectionPhonemes[phon]
                        Settings.SelectionPhonemes['w_'+phon] = Settings.SelectionPhonemes[phon]
                    except:
                        Settings.SelectionPhonemes[phon] = Settings.SelectionPhonemes['j_'+phon] = Settings.SelectionPhonemes['w_'+phon] = 0
            self.setValue(name, Settings.SelectionPhonemes)
        elif name == "__selection_lettres__":
            prop = value
            if not prop is None:
                selphon = [ph.split(':') for ph in prop.split(';')]
                Settings.SelectionLettres = dict([[ph[0], eval(ph[1])] for ph in selphon])
            self.setValue(name, Settings.SelectionLettres)
        elif name == "__point__":
            Settings.Point = int(value)
            self.setValue(name, Settings.Point)
        elif name == "__thread__":
            Settings.Threads = int(value)
            self.setValue(name, Settings.Threads)
        elif name == "__detection_phonemes__":
            Settings.Simple = int(value)
            self.setValue(name, Settings.Simple)
        elif name == "__syllo__":
            choix_syllo = int(value)
            Settings.Syllo = (choix_syllo%2, int(choix_syllo/10)%2)
            self.setValue(name, Settings.Syllo)
        elif name == "__alternate__":
            Settings.Alternate = int(value)
            self.setValue(name, Settings.Alternate)
        elif name == "__locale__":
            Settings.Locale = value
            self.setValue(name, Settings.Locale)
        elif name == "__profils__":
            Settings.LProfils = value.split(';')
            self.setValue(name, Settings.LProfils)
        elif name == "__fonctions_possibles__":
            Settings.FPossibles = value.split(':')
            self.setValue(name, Settings.FPossibles)
        elif name == "__selection_fonctions__":
            Settings.SelectionFonctions = value.split(':')
            self.setValue(name, Settings.SelectionFonctions)

    def setValue(self, name, value):
        """ set specified value. """
        cua = get_config_access(self.ctx, CONFIG_NODE, True)
        if cua is None:
            return
        
        try:
            if name == "__fonctions_possibles__":
                Settings.FPossibles = value
                cua.setPropertyValues(("__fonctions_possibles__",), (':'.join(value),))
            elif name == "__selection_fonctions__":
                Settings.SelectionFonctions = value
                for fct in Settings.FPossibles:
                    if fct in Settings.SelectionFonctions:
                        Settings.Fonctions[fct][2] = 1
                    else:
                        Settings.Fonctions[fct][2] = 0
            elif name == "__taille_icones__":
                Settings.TaillIco = value
                cua.setPropertyValues(("__taille_icones__",), (value,))
            elif name == "__selection_phonemes__":
                Settings.SelectionPhonemes = value
                cua.setPropertyValues(("__selection_phonemes__",), (';'.join([ph+':'+str(value[ph]) for ph in value.keys()]),))
            elif name == "__selection_lettres__":
                Settings.SelectionLettres = value
                cua.setPropertyValues(("__selection_lettres__",), (';'.join([ph+':'+str(value[ph]) for ph in value.keys()]),))
            elif name == "__template__":
                Settings.Template = value
                cua.setPropertyValues(("__template__",), (value,))
            elif name == "__point__":
                Settings.Point = value
                cua.setPropertyValues(("__point__",), (value,))
            elif name == "__thread__":
                Settings.Threads = value
                cua.setPropertyValues(("__thread__",), (value,))
            elif name == "__detection_phonemes__":
                Settings.Simple = value
                cua.setPropertyValues(("__detection_phonemes__",), (value,))
            elif name == "__syllo__":
                Settings.Syllo = value
                cua.setPropertyValues(("__syllo__",), (value[1]*10+value[0],))
            elif name == "__alternate__":
                Settings.Alternate = value
                cua.setPropertyValues(("__alternate__",), (value,))
            elif name == "__locale__":
                Settings.Locale = value
                cua.setPropertyValues(("__locale__",), (value,))
            elif name == "__profils__":
                Settings.LProfils = value
                cua.setPropertyValues(("__profils__",), (';'.join([prof for prof in value]),))
            else:
                Settings.Fonctions[name] = value.split(':')
                Settings.Fonctions[name][2] = eval(self.Fonctions[name][2])
                cua.setPropertyValues((name,), (value,))

            cua.commitChanges()
        except:
            LireCouleurLogBox('erreur Settings.setValue '+name+' '+str(value))
