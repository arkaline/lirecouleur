#!/usr/bin/env python
# -*- coding: UTF-8 -*-
from __future__ import unicode_literals

###################################################################################
# LireCouleur - outils d'aide à la lecture
#
# voir http://lirecouleur.arkaline.fr
#
# @author Marie-Pierre Brungard
# @version 5.3.0
# @since 2022
# https://forum.openoffice.org/en/forum/viewtopic.php?f=20&t=87057
#
# GNU General Public Licence (GPL) version 3
#
# LireCouleur is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.
# LireCouleur is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
# You should have received a copy of the GNU General Public License along with
# LireCouleur; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA  02111-1307  USA
###################################################################################

import os
import re
import logging
import sys
import codecs
import json

# configuration du niveau de log (DEBUG = le plus bas niveau ; CRITICAL = le plus haut niveau)
# logging.basicConfig(level = logging.INFO, format="%(asctime)s - %(funcName)s - %(lineno)d - %(levelname)s : %(message)s")

"""
    Correspondance entre le code SAMPA et le code LireCouleur
    référence : http://fr.wikipedia.org/wiki/Symboles_SAMPA_fran%C3%A7ais
"""
sampa2lc = {'p':'p', 'b':'b', 't':'t', 'd':'d', 'k':'k', 'g':'g', 'f':'f', 'v':'v',
's':'s', 'z':'z', 'S':'s^', 'Z':'g^', 'j':'j', 'm':'m', 'n':'n', 'J':'g~',
'N':'n~', 'l':'l', 'R':'r', 'w':'w', 'H':'y', 'i':'i', 'e':'e', 'E':'e^',
'a':'a', 'A':'a', 'o':'o', 'O':'o_ouvert', 'u':'u', 'y':'y', '2':'x^', '9':'x',
'@':'q', 'e~':'e~', 'a~':'a~', 'o~':'o~', '9~':'x~', '#':'#'}

"""
    Constantes LireCouleur
"""

class ConstLireCouleur:
    # différentes configurations de marquage des syllabes
    SYLLABES_LC = 0
    SYLLABES_STD = 1
    SYLLABES_ORALES = 1
    SYLLABES_ECRITES = 0

    # prononciation différente entre l'Europe et le Canada
    MESTESSESLESDESCES = {'':'e_comp', 'fr':'e_comp', 'fr_CA':'e^_comp'}


######################################################################################
# Classe de gestion du dictionnaire de décodage
######################################################################################
class LCDictionnary(object):
    _loaded = False
    _dict = {}
    _filename = ""
    
    def __init__(self, filename=""):
        if not LCDictionnary._loaded:
            self.load(filename)

    def load(self, filename=""):
        if LCDictionnary._loaded:
            return
        LCDictionnary._filename = filename
        LCDictionnary._loaded = True
        if not os.path.isfile(filename):
            return
        ff = codecs.open(filename, "r", "utf_8_sig", errors="replace")
        for line in ff:
            if line.isspace():
                continue
            line = line.rstrip('\r\n')
            if not line.startswith('#'):
                temp = re.split('[;\t]', line)
                if len(temp) > 1:
                    LCDictionnary._dict[temp[0]] = temp[1:]
                    if len(temp[1:]) < 2:
                        LCDictionnary._dict[temp[0]].append('')
        ff.close()

    def save(self):
        if not LCDictionnary._loaded:
            return
        if len(LCDictionnary._filename) > 0:
            ff = codecs.open(LCDictionnary._filename, "w", "utf_8_sig", errors="replace")
            for key in LCDictionnary._dict.keys():
                line = key + ';' + LCDictionnary._dict[key][0] + ';' + LCDictionnary._dict[key][1] + '\n'
                ff.write(line)
            ff.close()

    def getKeys(self):
        if not LCDictionnary._loaded:
            self.load()
        return LCDictionnary._dict.keys()

    def getEntry(self, key):
        try:
            return LCDictionnary._dict[key]
        except:
            return ['', '']

    def delEntry(self, key):
        try:
            del LCDictionnary._dict[key]
            self.save()
        except:
            pass
    
    
    def setEntry(self, key, phon, syll):
        try:
            LCDictionnary._dict[key] = [phon, syll]
            self.save()
        except:
            pass

#########################################################################################################
#########################################################################################################
#
#    Cette partie du code est destinée au traitement des textes pour en extraires des
#    phonèmes et des syllabes.
#
#                                    @@@@@@@@@@@@@@@@@@@@@@
#
#########################################################################################################
#########################################################################################################

# configuration du niveau de log (DEBUG = le plus bas niveau ; CRITICAL = le plus haut niveau)
# try:
#    flog = os.path.dirname(os.path.abspath(__file__))+os.sep+"lirecouleur.log"
# except:
#    flog = "lirecouleur.log"
# logging.basicConfig(level = logging.ERROR,
        # format="%(asctime)s - %(funcName)s - %(lineno)d - %(levelname)s : %(message)s",
        # filename = flog)


###################################################################################
# reste du passage à Python3 pour le traitement des unicode
###################################################################################
if not 'unicode' in dir(__builtins__):
    def unicode(obj, __dummy): return str(obj)

def u(txt):
    try:
        return unicode(txt, 'utf-8')
    except:
        return txt

###################################################################################
# Liste des mots non correctement traités :
# agenda, consensus, référendum
###################################################################################


###################################################################################
# Les phonèmes sont codés en voyelles (v), consonnes (c) et semi-voyelles (s)
###################################################################################
syllaphon = json.loads("""
{"v":["a","q","q_caduc","i","o","o_comp","o_ouvert","u","y","e","e_comp","e^","e^_comp","a~","e~","x~","o~","x","x^","wa","w5"],"c":["p","t","k","b","d","g","f","f_ph","s","s^","v","z","z^","l","r","m","n","k_qu","z^_g","g_u","s_c","s_t","z_s","ks","gz"],"s":["j","g~","n~","w"],"#":["#","verb_3p"]}
""")

#########################################################################################################
# Alphabet phonétique ascii : voir http://www.icp.inpg.fr/ICP/avtts/phon.fr.html
# Outil inestimable : http://www.lexique.org/moteur
#########################################################################################################

###################################################################################
# Ensemble de verbes qui se terminent par -ier // attention : pas d'accents !!
###################################################################################
verbes_ier = json.loads("""
["affilier","allier","allier","amnistier","amplifier","anesthesier","apparier","approprier","apprecier","asphyxier","associer","atrophier","authentifier","autographier","autopsier","balbutier","bonifier","beatifier","beneficier","betifier","calligraphier","calomnier","carier","cartographier","certifier","charrier","chier","choregraphier","chosifier","chatier","clarifier","classifier","cocufier","codifier","colorier","communier","conchier","concilier","confier","congedier","contrarier","copier","crier","crucifier","dactylographier","differencier","disgracier","disqualifier","dissocier","distancier","diversifier","domicilier","decrier","dedier","defier","deifier","delier","demarier","demultiplier","demystifier","denazifier","denier","deplier","deprecier","dequalifier","devier","envier","estropier","excommunier","exemplifier","exfolier","expatrier","expier","exproprier","expedier","extasier","falsifier","fier","fluidifier","fortifier","frigorifier","fructifier","gazeifier","glorifier","gracier","gratifier","horrifier","humidifier","humilier","identifier","incendier","ingenier","initier","injurier","intensifier","inventorier","irradier","justifier","licencier","lier","liquefier","lubrifier","magnifier","maleficier","manier","marier","mendier","modifier","momifier","mortifier","multiplier","mystifier","mythifier","mefier","nier","notifier","negocier","obvier","officier","opacifier","orthographier","oublier","pacifier","palinodier","pallier","parier","parodier","personnifier","photocopier","photographier","plagier","planifier","plastifier","plier","polycopier","pontifier","prier","privilegier","psalmodier","publier","purifier","putrefier","pepier","petrifier","qualifier","quantifier","radier","radiographier","rallier","ramifier","rapatrier","rarefier","rassasier","ratifier","razzier","recopier","rectifier","relier","remanier","remarier","remercier","remedier","renier","renegocier","replier","republier","requalifier","revivifier","reverifier","rigidifier","reconcilier","recrier","reexpedier","refugier","repertorier","repudier","resilier","reunifier","reedifier","reetudier","sacrifier","salarier","sanctifier","scier","signifier","simplifier","skier","solidifier","soucier","spolier","specifier","statufier","strier","stupefier","supplicier","supplier","serier","terrifier","tonifier","trier","tumefier","typographier","telegraphier","unifier","varier","versifier","vicier","vitrifier","vivifier","verifier","echographier","ecrier","edifier","electrifier","emulsifier","epier","etudier"]
""")

###################################################################################
# Ensemble de verbes qui se terminent par -mer
###################################################################################
verbes_mer = json.loads("""
["abimer","acclamer","accoutumer","affamer","affirmer","aimer","alarmer","allumer","amalgamer","animer","armer","arrimer","assommer","assumer","blasphemer","blamer","bramer","brimer","calmer","camer","carmer","charmer","chloroformer","chomer","clamer","comprimer","confirmer","conformer","consommer","consumer","costumer","cramer","cremer","damer","diffamer","diplomer","decimer","declamer","decomprimer","deformer","degommer","denommer","deplumer","deprimer","deprogrammer","desaccoutumer","desarmer","desinformer","embaumer","embrumer","empaumer","enfermer","enflammer","enfumer","enrhumer","entamer","enthousiasmer","entraimer","envenimer","escrimer","estimer","exclamer","exhumer","exprimer","fantasmer","fermer","filmer","flemmer","former","frimer","fumer","gendarmer","germer","gommer","grammer","grimer","groumer","humer","imprimer","infirmer","informer","inhumer","intimer","lamer","limer","legitimer","mimer","mesestimer","nommer","opprimer","palmer","parfumer","parsemer","paumer","plumer","pommer","primer","proclamer","programmer","preformer","prenommer","presumer","pamer","perimer","rallumer","ramer","ranimer","refermer","reformer","refumer","remplumer","renfermer","renommer","rentamer","reprogrammer","ressemer","retransformer","rimer","rythmer","reaccoutumer","reaffirmer","reanimer","rearmer","reassumer","reclamer","reformer","reimprimer","reprimer","resumer","retamer","semer","slalomer","sommer","sublimer","supprimer","surestimer","surnommer","tramer","transformer","trimer","zoomer","ecremer","ecumer","elimer"]
""")

###################################################################################
# Ensemble de mots qui se terminent par -ent
###################################################################################
mots_ent = json.loads("""
["absent","abstinent","accent","accident","adhérent","adjacent","adolescent","afférent","agent","ambivalent","antécédent","apparent","arborescent","ardent","ardent","argent","arpent","astringent","auvent","avent","cent","chiendent","client","coefficient","cohérent","dent","différent","diligent","dissident","divergent","dolent","décadent","décent","déficient","déférent","déliquescent","détergent","excipient","fervent","flatulent","fluorescent","fréquent","féculent","gent","gradient","grandiloquent","immanent","imminent","impatient","impertinent","impotent","imprudent","impudent","impénitent","incandescent","incident","incohérent","incompétent","inconscient","inconséquent","incontinent","inconvénient","indifférent","indigent","indolent","indulgent","indécent","ingrédient","inhérent","inintelligent","innocent","insolent","intelligent","interférent","intermittent","iridescent","lactescent","latent","lent","luminescent","malcontent","mécontent","occident","omnipotent","omniprésent","omniscient","onguent","opalescent","opulent","orient","paravent","parent","patent","patient","permanent","pertinent","phosphorescent","polyvalent","pourcent","proéminent","prudent","précédent","présent","prévalent","pschent","purulent","putrescent","pénitent","quotient","relent","récent","récipient","récurrent","référent","régent","rémanent","réticent","sanguinolent","sergent","serpent","somnolent","souvent","spumescent","strident","subconscient","subséquent","succulent","tangent","torrent","transparent","trident","truculent","tumescent","turbulent","turgescent","urgent","vent","ventripotent","violent","virulent","effervescent","efficient","effluent","engoulevent","entregent","escient","event","excédent","expédient","éloquent","éminent","émollient","évanescent","évent"]
""")

verbes_enter = json.loads("""
["absenter","accidenter","agrémenter","alimenter","apparenter","cimenter","contenter","complimenter","bonimenter","documenter","patienter","parlementer","ornementer","supplémenter","argenter","éventer","supplémenter","tourmenter","violenter","arpenter","serpenter","coefficienter","argumenter","présenter"]
""")

###################################################################################
# Exceptions dans lesquelles -ier se prononce [ièR]
###################################################################################
exceptions_final_er = json.loads("""
    ["amer","cher","hier","mer","coroner","charter","cracker","hiver","chester","doppler","cascher","bulldozer","cancer","carter","geyser","cocker","pullover","alter","aster","fer","ver","diver","perver","enfer","traver","univer","cuiller","container","cutter","révolver","super","master","enver"]
""")

###################################################################################
# Le ai final se prononce è et non pas é
###################################################################################
possibles_nc_ai_final = json.loads("""
    ["balai", "brai", "chai", "déblai", "délai", "essai", "frai", "geai", "lai", "mai", "minerai", "papegai", "quai", "rai", "remblai"]
""")

###################################################################################
# Conjugaison du verbe avoir : eu = [u]
###################################################################################
possibles_avoir = json.loads("""
    ["eu", "eue", "eues", "eus", "eut", "eûmes", "eûtes", "eurent", "eusse", "eusses", "eût", "eussions", "eussiez", "eussent"]
""")

###################################################################################
# Ensemble de mots pour lesquels le s final est prononcé
###################################################################################
mots_s_final = json.loads("""
    ["abribus","airbus","autobus","bibliobus","bus","nimbus","gibus","microbus","minibus","mortibus","omnibus","oribus","pédibus","quibus","rasibus","rébus","syllabus","trolleybus","virus","antivirus","anus","asparagus","médius","autofocus","focus","benedictus","bonus","campus","cirrus","citrus","collapsus","consensus","corpus","crochus","crocus","crésus","cubitus","humérus","diplodocus","eucalyptus","erectus","hypothalamus","mordicus","mucus","stratus","nimbostratus","nodus","modus","opus","ours","papyrus","plexus","plus","processus","prospectus","lapsus","prunus","quitus","rétrovirus","sanctus","sinus","solidus","liquidus","stimulus","stradivarius","terminus","tonus","tumulus","utérus","versus","détritus","ratus","couscous","burnous","tous","anis","bis","anubis","albatros","albinos","calvados","craignos","mérinos","rhinocéros","tranquillos","tétanos","os","alias","atlas","hélas","madras","sensas","tapas","trias","vasistas","hypocras","gambas","as","biceps","quadriceps","chips","relaps","forceps","schnaps","laps","oups","triceps","princeps","tricératops"]
""")

###################################################################################
# Ensemble de mots pour lesquels le t final est prononcé
###################################################################################
mots_t_final = json.loads("""
    ["accessit","cet","but","diktat","kumquat","prurit","affidavit","dot","rut","audit","exeat","magnificat","satisfecit","azimut","exit","mat","scorbut","brut","fiat","mazout","sinciput","cajeput","granit","net","internet","transat","sept","chut","huit","obit","transit","coït","incipit","occiput","ut","comput","introït","pat","zut","déficit","inuit","prétérit","gadget","kilt","kit","scout","fret"]
""")

###################################################################################
# Ensemble de mots pour lesquels le ien final se prononce [in]
###################################################################################
exceptions_final_tien = json.loads("""
    ["chrétien", "entretien", "kantien", "proustien", "soutien"]
""")

###################################################################################
# Règle spécifique de traitement des successions de lettres finales 'ient'
#     sert à savoir si la séquence 'ient' se prononce [i][#] ou [j][e~]
###################################################################################
def regle_ient(mot, pos_mot):
    m = re.match('[bcçdfghjklnmpqrstvwxz]ient', mot[-5:])
    if m == None or (pos_mot < len(mot[:-4])):
        # le mot ne se termine pas par 'ient' (précédé d'une consonne)
        # ou alors on est en train d'étudier une lettre avant la terminaison en 'ient'
        return False

    # il faut savoir si le mot est un verbe dont l'infinitif se termine par 'ier' ou non
    pseudo_infinitif = mot[:-2] + 'r'
    if pseudo_infinitif in verbes_ier:
        logging.info("func regle_ient : " + mot + " (" + pseudo_infinitif + ")")
        return True
    pseudo_infinitif = texte_sans_accent(pseudo_infinitif)
    if len(pseudo_infinitif) > 1 and pseudo_infinitif[1] == '@':
        # mot précédé d'un déterminant élidé - codage de l'apostrophe : voir pretraitement_texte
        pseudo_infinitif = pseudo_infinitif[2:]
    if pseudo_infinitif in verbes_ier:
        logging.info("func regle_ient : " + mot + " (" + pseudo_infinitif + ")")
        return True
    return False


###################################################################################
# Règle spécifique de traitement des successions de lettres '*ent'
#     sert à savoir si le mot figure dans les mots qui se prononcent a~ à la fin
###################################################################################
def regle_mots_ent(mot, pos_mot):
    m = re.match('^[bcdfghjklmnpqrstvwxz]ent(s?)$', mot)
    if m != None:
        logging.info("func regle_mots_ent : " + mot + " -- mot commencant par une consonne et terminé par 'ent'")
        return True

    # il faut savoir si le mot figure dans la liste des adverbes ou des noms répertoriés
    comparateur = mot
    if mot[-1] == 's':
        comparateur = mot[:-1]
    if pos_mot + 2 < len(comparateur):
        return False

    if len(comparateur) > 1 and comparateur[1] == '@':
        # mot précédé d'un déterminant élidé - codage de l'apostrophe : voir pretraitement_texte
        comparateur = comparateur[2:]

    # comparaison directe avec la liste de mots où le 'ent' final se prononce [a~]
    if comparateur in mots_ent:
        logging.info("func regle_mots_ent : " + mot + " -- mot répertorié")
        return True

    # comparaison avec la liste de verbes qui se terminent par 'enter'
    pseudo_verbe = comparateur + 'er'
    if pseudo_verbe in verbes_enter:
        logging.info("func regle_mots_ent : " + mot + " -- verbe 'enter'")
        return True

    return False


###################################################################################
# Règle spécifique de traitement des successions de lettres 'ment'
#     sert à savoir si le mot figure dans les mots qui se prononcent a~ à la fin
###################################################################################
def regle_ment(mot, pos_mot):
    m = re.match('ment', mot[-4:])
    if m == None or (pos_mot < len(mot[:-3])):
        # le mot ne se termine pas par 'ment'
        # ou alors on est en train d'étudier une lettre avant la terminaison en 'ment'
        return False

    # il faut savoir si le mot figure dans la liste des verbes terminés par -mer
    pseudo_infinitif = texte_sans_accent(mot[:-2] + 'r')
    if len(pseudo_infinitif) > 1 and pseudo_infinitif[1] == '@':
        # mot précédé d'un déterminant élidé - codage de l'apostrophe : voir pretraitement_texte
        pseudo_infinitif = pseudo_infinitif[2:]
    if pseudo_infinitif in verbes_mer:
        return False

    # dernier test : le verbe dormir (ils/elles dorment)
    if len(mot) > 6:
        if re.match('dorment', mot[-7:]) != None:
            return False
    logging.info("func regle_ment : " + mot + " (" + pseudo_infinitif + ")")
    return True


def regle_verbe_mer(mot, pos_mot):
    """L'inverse de la règle ci-dessus ou presque"""
    m = re.match('ment', mot[-4:])
    if m == None or (pos_mot < len(mot[:-3])):
        # le mot ne se termine pas par 'ment'
        # ou alors on est en train d'étudier une lettre avant la terminaison en 'ment'
        return False

    return not regle_ment(mot, pos_mot)


###################################################################################
# Règle spécifique de traitement des successions de lettres finales 'er'
#     sert à savoir si le mot figure dans la liste des exceptions
###################################################################################
def regle_er(mot, pos_mot):
    # prendre le mot au singulier uniquement
    m_sing = mot
    if mot[-1] == 's':
        m_sing = mot[:-1]

    if len(m_sing) > 1 and m_sing[1] == '@':
        # mot précédé d'un déterminant élidé - codage de l'apostrophe : voir pretraitement_texte
        m_sing = m_sing[2:]

    # tester la terminaison
    m = re.match('er', m_sing[-2:])
    if m == None or (pos_mot < len(m_sing[:-2])):
        # le mot ne se termine pas par 'er'
        # ou alors on est en train d'étudier une lettre avant la terminaison en 'er'
        return False

    # il faut savoir si le mot figure dans la liste des exceptions
    if m_sing in exceptions_final_er:
        logging.info("func regle_er : " + mot + " -- le mot n'est pas une exception comme 'amer' ou 'cher'")
        return True
    return False


###################################################################################
# Règle spécifique de traitement des noms communs qui se terminent par 'ai'
#   Dans les verbes terminés par 'ai', le phonème est 'é'
#   Dans les noms communs terminés par 'ai', le phonème est 'ê'
###################################################################################
def regle_nc_ai_final(mot, pos_mot):
    m_seul = mot
    if len(m_seul) > 1 and m_seul[1] == '@':
        # mot précédé d'un déterminant élidé - codage de l'apostrophe : voir pretraitement_texte
        m_seul = m_seul[2:]

    if m_seul in possibles_nc_ai_final:
        res = (pos_mot == len(mot) - 1)
        logging.info("func regle_nc_ai_final : " + mot + " -- " + str(res))
        return res
    return False


###################################################################################
# Règle spécifique de traitement des successions de lettres 'eu('
#     Sert à savoir si le mot est le verbe avoir conjugué (passé simple, participe
#   passé ou subjonctif imparfait
###################################################################################
def regle_avoir(mot, pos_mot):
    if mot in possibles_avoir:
        res = (pos_mot < 2)
        logging.info("func regle_avoir : " + mot + " -- " + str(res))
        return res
    return False


###################################################################################
# Règle spécifique de traitement des mots qui se terminent par "us".
# Pour un certain nombre de ces mots, le 's' final se prononce.
###################################################################################
def regle_s_final(mot, __pos_mot):
    m_seul = mot
    if len(m_seul) > 1 and m_seul[1] == '@':
        # mot précédé d'un déterminant élidé - codage de l'apostrophe : voir pretraitement_texte
        m_seul = m_seul[2:]

    if m_seul in mots_s_final:
        logging.info("func regle_s_final : " + m_seul + " -- mot avec un 's' final qui se prononce")
        return True
    return False


###################################################################################
# Règle spécifique de traitement des mots qui se terminent par la lettre "t" prononcée.
###################################################################################
def regle_t_final(mot, __pos_mot):
    # prendre le mot au singulier uniquement
    m_sing = mot
    if mot[-1] == 's':
        m_sing = mot[:-1]

    if len(m_sing) > 1 and m_sing[1] == '@':
        # mot précédé d'un déterminant élidé - codage de l'apostrophe : voir pretraitement_texte
        m_sing = m_sing[2:]

    if m_sing in mots_t_final:
        logging.info("func regle_t_final : " + mot + " -- mot avec un 't' final qui se prononce")
        return True
    return False


###################################################################################
# Règle spécifique de traitement de quelques mots qui se terminent par 'tien' et
# dans lesquels le 't' se prononce [t]
###################################################################################
def regle_tien(mot, pos_mot):
    # prendre le mot au singulier uniquement
    m_sing = mot
    if m_sing[-1] == 's':
        m_sing = mot[:-1]

    # tester la terminaison
    m = re.match('tien', m_sing[-4:])
    if m == None or (pos_mot < len(m_sing[:-4])):
        # le mot ne se termine pas par 'tien'
        # ou alors on est en train d'étudier une lettre avant la terminaison en 'tien'
        return False

    # il faut savoir si le mot figure dans la liste des exceptions
    if m_sing in exceptions_final_tien:
        logging.info("func regle_tien : " + mot + " -- mot où le 't' de 'tien' se prononce 't'")
        return True
    return False


###################################################################################
# Ensemble des règles d'extraction des phonèmes
# '*' signifie 'suivi par n'importe quelle lettre
# '@' signifie 'dernière lettre du mot
#
# format de l'automate:
#        'lettre': [[règles l'ordre où elles doivent être déclenchées],[liste des règles]]
#
#     ATTENTION. Il faut faire attention à l'ordre de précédence des règles. Plusieurs règles peuvent
#    en effet s'appliquer pour une même succession de lettres. Il faut ranger les règles de la plus
#    spécifique à la plus générale.
#
# format d'une règle :
#        'nom_de_la_regle': [motif, phoneme, pas]
#
#    motif : il s'agit d'une expression régulière qui sert à tester les successions de lettres qui suivent
#        la lettre en cours de traitement dans le mot et les successions de lettres qui précèdent la lettre
#        en cours de traitement.
#    phoneme : le nom du phonème codé selon le format ascii décrit dans
#        http://www.icp.inpg.fr/ICP/avtts/phon.fr.html
#    pas : le nombre de lettres à lire à partir de la lettre courante si le motif a été reconnu
#        dans le mot de part et d'autre de la lettre en cours de traitement.
#
###################################################################################

## https://jsonformatter.org/
autom = json.loads("""
{"'":[[],{"*":[{},"#",1],"@":[{},"#",1]}],"@":[[],{"*":[{},"#",1],"@":[{},"#",1]}],"_":[[],{"*":[{},"#",1],"@":[{},"#",1]}],"a":[["u","il","in","nc_ai_fin","ai_fin","i","n","m","nm","y_except","y"],{"*":[{},"a",1],"ai_fin":[{"+":"i$"},"e_comp",2],"i":[{"+":"[iî]"},"e^_comp",2],"il":[{"+":"il($|l)"},"a",1],"in":[{"+":"i[nm]([bcçdfghjklnmpqrstvwxz]|$)"},"e~",3,"#comment# toute succession ain aim suivie d une consonne ou d une fin de mot"],"m":[{"+":"m[mbp]"},"a~",2,"#comment# règle du m devant m, b, p"],"n":[{"+":"n[bcçdfgjklmpqrstvwxz]"},"a~",2],"nc_ai_fin":["regle_nc_ai_final","e^_comp",2],"nm":[{"+":"n(s?)$"},"a~",2],"u":[{"+":"u"},"o_comp",2],"y":[{"+":"y"},"e^_comp",1],"y_except":[{"+":"y","-":"(^b|cob|cip)"},"a",1,"#comment# exception : baye, cobaye"]}],"b":[["b","plomb"],{"*":[{},"b",1],"b":[{"+":"b"},"b",2],"plomb":[{"+":"(s?)$","-":"plom"},"#",1,"#comment# le b à la fin de plomb ne se prononce pas"]}],"c":[["eiy","choeur_1","choeur_2","chor","psycho","brachio","cheo","chest","chiro","chlo_chlam","chr","h","erc_orc","cisole","c_muet_fin","onc_donc","nc_muet_fin","_spect","_inct","cciey","cc","apostrophe"],{"*":[{},"k",1],"@":["","k",1],"_inct":[{"+":"t(s?)$","-":"in"},"#",1,"#comment# instinct, succinct, distinct"],"_spect":[{"+":"t(s?)$","-":"spe"},"#",1,"#comment# respect, suspect, aspect"],"apostrophe":[{"+":"('|’)"},"s",2,"#comment# apostrophe"],"brachio":[{"+":"hio","-":"bra"},"k",2,"#comment# brachiosaure, brachiocéphale"],"c_muet_fin":[{"+":"(s?)$","-":"taba|accro"},"#",1,"#comment# exceptions traitées : tabac, accroc"],"cc":[{"+":"c"},"k",2,"#comment# accorder, accompagner"],"cciey":[{"+":"c[eiyéèêëîï]"},"k",1,"#comment# accident, accepter, coccyx"],"cheo":[{"+":"héo"},"k",2,"#comment# archéo..., trachéo..."],"chest":[{"+":"hest"},"k",2,"#comment# orchestre et les mots de la même famille"],"chiro":[{"+":"hiro[p|m]"},"k",2,"#comment# chiroptère, chiromancie"],"chlo_chlam":[{"+":"hl(o|am)"},"k",2,"#comment# chlorure, chlamyde"],"choeur_1":[{"+":"hoe"},"k",2],"choeur_2":[{"+":"hœ"},"k",2],"chor":[{"+":"hor"},"k",2,"#comment# tous les choral, choriste... exceptions non traitées : chorizo, maillechort"],"chr":[{"+":"hr"},"k",2,"#comment# de chrétien à synchronisé"],"cisole":[{"+":"$","-":"^"},"s_c",1,"#comment# exemple : c est"],"eiy":[{"+":"[eiyéèêëîï]"},"s_c",1],"erc_orc":[{"+":"(s?)$","-":"[e|o]r"},"#",1,"#comment# clerc, porc,"],"h":[{"+":"h"},"s^",2],"nc_muet_fin":[{"+":"(s?)$","-":"n"},"#",1,"#comment# exceptions traitées : tous les mots terminés par *nc"],"onc_donc":[{"-":"^on|^don"},"k",1,"#comment# non exceptions traitées : onc, donc"],"psycho":[{"+":"ho","-":"psy"},"k",2,"#comment# tous les psycho quelque chose"]}],"d":[["d","aujourdhui","disole","except","dmuet","apostrophe"],{"*":[{},"d",1],"apostrophe":[{"+":"('|’)"},"d",2,"#comment# apostrophe"],"aujourdhui":[{"-":"aujour"},"d",1,"#comment# aujourd hui"],"d":[{"+":"d"},"d",2],"disole":[{"+":"$","-":"^"},"d",1,"#comment# exemple : d abord"],"dmuet":[{"+":"(s?)$"},"#",1,"#comment# un d suivi éventuellement d un s ex. : retards"],"except":[{"+":"(s?)$","-":"(aï|oue)"},"d",1,"#comment# aïd, caïd, oued"]}],"e":[["conj_v_ier","uient","ien","ien_2","een","except_en_1","except_en_2","_ent","clef","hier","adv_emment_fin","ment","imparfait","verbe_3_pluriel","au","avoir","monsieur","jeudi","jeu_","eur","eu","eu_accent_circ","in","eil","y","iy","ennemi","enn_debut_mot","dessus_dessous","et","cet","t_final","eclm_final","est_1","est_2","es_1","es_2","drz_final","n","adv_emment_a","femme","lemme","em_gene","nm","tclesmesdes","que_isole","que_gue_final","jtcnslemede","jean","ge","eoi","ex","ef","reqquechose","except_evr","2consonnes","abbaye","e_muet","e_caduc","e_deb"],{"*":[{},"q",1],"except_evr":[{"+":"vr"},"q",1,"#comment# chevrier, chevron, chevreuil..."],"2consonnes":[{"+":"[bcçdfghjklmnpqrstvwxz]{2}"},"e^_comp",1,"#comment# e suivi de 2 consonnes se prononce è"],"@":["","q_caduc",1],"_ent":["regle_mots_ent","a~",2,"#comment# quelques mots (adverbes ou noms) terminés par ent"],"abbaye":[{"+":"(s?)$","-":"abbay"},"#",1,"#comment# ben oui..."],"adv_emment_a":[{"+":"mment"},"a",1,"#comment# adverbe avec -emment => son [a]"],"adv_emment_fin":[{"+":"nt","-":"emm"},"a~",2,"#comment# adverbe avec -emment => se termine par le son [a~]"],"au":[{"+":"au"},"o_comp",3],"avoir":["regle_avoir","y",2],"cet":[{"+":"[t]$","-":"^c"},"e^_comp",1,"#comment# cet"],"clef":[{"+":"f","-":"cl"},"e_comp",2,"#comment# une clef"],"conj_v_ier":["regle_ient","#",3,"#comment# verbe du 1er groupe terminé par -ier conjugué à la 3ème pers du pluriel"],"dessus_dessous":[{"+":"ss(o?)us","-":"d"},"q",1,"#comment# dessus, dessous : e = e"],"drz_final":[{"+":"[drz](s?)$"},"e_comp",2,"#comment# e suivi d un d,r ou z en fin de mot done le son [e]"],"e_caduc":[{"+":"(s?)$","-":"[bcçdfghjklmnpqrstvwxzy]"},"q_caduc",1,"#comment# un e suivi éventuellement d un s et précédé d une consonne ex. : correctes"],"e_deb":[{"-":"^"},"q",1,"#comment# par défaut, un e en début de mot se prononce [q]"],"e_muet":[{"+":"(s?)$","-":"[aeiouéèêà]"},"#",1,"#comment# un e suivi éventuellement d un s et précédé d une voyelle ou d un g ex. : pie, geai"],"eclm_final":[{"+":"[clm](s?)$"},"e^_comp",1,"#comment# donne le son [e^] et le l ou le c se prononcent (ex. : miel, sec)"],"een":[{"+":"n(s?)$","-":"é"},"e~",2,"#comment# les mots qui se terminent par -éen"],"ef":[{"+":"[bf](s?)$"},"e^",1,"#comment# e suivi d un f ou d un b en fin de mot se prononce è"],"eil":[{"+":"il"},"e^_comp",1],"em_gene":[{"+":"m[bcçdfghjklmnpqrstvwxz]"},"a~",2,"#comment# em cas général => son [a~]"],"enn_debut_mot":[{"+":"nn","-":"^"},"a~",2,"#comment# enn en début de mot se prononce en"],"ennemi":[{"+":"nnemi","-":"^"},"e^_comp",1,"#comment# ennemi est l exception ou enn en début de mot se prononce èn (cf. enn_debut_mot)"],"eoi":[{"+":"oi"},"#",1,"#comment# un e suivi de oi ; ex. : asseoir"],"es_1":[{"+":"s$","-":"^"},"e^_comp",2],"es_2":[{"+":"s$","-":"@"},"e^_comp",2],"est_1":[{"+":"st$","-":"^"},"e^_comp",3],"est_2":[{"+":"st$","-":"@"},"e^_comp",3],"et":[{"+":"t$","-":"^"},"e_comp",2],"eu":[{"+":"u"},"x",2],"eu_accent_circ":[{"+":"û"},"x^",2],"eur":[{"+":"ur"},"x",2],"ex":[{"+":"x"},"e^",1,"#comment# e suivi d un x se prononce è"],"except_en_1":[{"+":"n(s?)$","-":"exam|mino|édu"},"e~",2,"#comment# exceptions des mots où le en final se prononce [e~] (héritage latin)"],"except_en_2":[{"+":"n(s?)$","-":"[ao]ï"},"e~",2,"#comment# païen, hawaïen, tolstoïen"],"femme":[{"+":"mm","-":"f"},"a",1,"#comment# femme et ses dérivés => son [a]"],"ge":[{"+":"[aouàäôâ]","-":"g"},"#",1,"#comment# un e précédé d un g et suivi d une voyelle ex. : cageot"],"hier":["regle_er","e^_comp",1,"#comment# encore des exceptions avec les mots terminés par er prononcés R"],"ien":[{"+":"n([bcçdfghjklpqrstvwxz]|$)","-":"[bcdlmrstvh]i"},"e~",2,"#comment# certains mots avec ien => son [e~]"],"ien_2":[{"+":"n([bcçdfghjklpqrstvwxz]|$)","-":"ï"},"e~",2,"#comment# certains mots avec ien => son [e~]"],"imparfait":[{"+":"nt$","-":"ai"},"verb_3p",3,"#comment# imparfait à la 3ème personne du pluriel"],"in":[{"+":"i[nm]([bcçdfghjklnmpqrstvwxz]|$)"},"e~",3,"#comment# toute succession ein eim suivie d une consonne ou d une fin de mot"],"iy":[{"+":"[iy]"},"e^_comp",2],"jean":[{"+":"an","-":"j"},"#",1,"#comment# jean"],"jeu_":[{"+":"u","-":"j"},"x",2,"#comment# tous les jeu* sauf jeudi"],"jeudi":[{"+":"udi","-":"j"},"x^",2,"#comment# jeudi"],"jtcnslemede":[{"+":"$","-":"^[jtcnslmd]"},"q",1,"#comment# je, te, me, le, se, de, ne"],"lemme":[{"+":"mm","-":"l"},"e^_comp",1,"#comment# lemme et ses dérivés => son [e^]"],"ment":["regle_ment","a~",2,"#comment# on considère que les mots terminés par -ment se prononcent [a~] sauf s il s agit d un verbe"],"monsieur":[{"+":"ur","-":"si"},"x^",2],"n":[{"+":"n[bcçdfghjklmpqrstvwxz]"},"a~",2],"nm":[{"+":"[nm]$"},"a~",2],"que_gue_final":[{"+":"(s?)$","-":"[gq]u"},"q_caduc",1,"#comment# que ou gue final"],"que_isole":[{"+":"$","-":"^qu"},"q",1,"#comment# que isolé"],"reqquechose":[{"+":"[bcçdfghjklmnpqrstvwxz](h|l|r)","-":"r"},"q",1,"#comment# re-quelque chose : le e se prononce e"],"t_final":[{"+":"[t]$"},"e^_comp",2,"#comment# donne le son [e^] et le t ne se prononce pas"],"tclesmesdes":[{"+":"s$","-":"^[tcslmd]"},"e_comp",2,"#comment# mes, tes, ces, ses, les"],"uient":[{"+":"nt$","-":"ui"},"#",3,"#comment# enfuient, appuient, fuient, ennuient, essuient"],"verbe_3_pluriel":[{"+":"nt$"},"q_caduc",1,"#comment# normalement, pratiquement tout le temps verbe à la 3eme personne du pluriel"],"y":[{"+":"y[aeiouéèêààäôâ]"},"e^_comp",1]}],"f":[["f","oeufs"],{"*":[{},"f",1],"f":[{"+":"f"},"f",2],"oeufs":[{"+":"s","-":"(oeu|œu)"},"#",1,"#comment# oeufs et boeufs"]}],"g":[["g","ao","eiy","aiguille","u_consonne","u","n","vingt","g_muet_oin","g_muet_our","g_muet_an","g_muet_fin"],{"*":[{},"g",1],"aiguille":[{"+":"u","-":"ai"},"g",1,"#comment# encore une exception : aiguille et ses dérivés"],"ao":[{"+":"a|o"},"g",1],"eiy":[{"+":"[eéèêëïiy]"},"z^_g",1,"#comment# un g suivi de e,i,y se prononce [z^]"],"g":[{"+":"g"},"g",2],"g_muet_an":[{"+":"(s?)$","-":"(s|^ét|^r)an"},"#",1,"#comment# sang, rang, étang"],"g_muet_fin":[{"-":"lon|haren"},"#",1,"#comment# pour traiter les exceptions : long, hareng"],"g_muet_oin":[{"-":"oi(n?)"},"#",1,"#comment# un g précédé de oin ou de oi ne se prononce pas ; ex. : poing, doigt"],"g_muet_our":[{"-":"ou(r)"},"#",1,"#comment# un g précédé de our ou de ou ne se prononce pas ; ex. : bourg"],"n":[{"+":"n"},"n~",2],"u":[{"+":"u"},"g_u",2],"u_consonne":[{"+":"u[bcçdfghjklmnpqrstvwxz]"},"g",1,"#comment# gu suivi d une consonne se prononce [g][y]"],"vingt":[{"+":"t","-":"vin"},"#",1,"#comment# vingt"]}],"h":[[],{"*":[{},"#",1]}],"i":[["ing","n","m","nm","prec_2cons","lldeb","vill","mill","tranquille","ill","@ill","@il","ll","ui","ient_1","ient_2","ie"],{"*":[{},"i",1],"@il":[{"+":"l(s?)$","-":"[aeou]"},"j",2,"#comment# par défaut précédé d une voyelle et suivi de l donne le son [j]"],"@ill":[{"+":"ll","-":"[aeo]"},"j",3,"#comment# par défaut précédé d une voyelle et suivi de ll donne le son [j]"],"ie":[{"+":"e(s)?$"},"i",1,"#comment# mots terminés par -ie(s|nt)"],"ient_1":["regle_ient","i",1,"#comment# règle spécifique pour différencier les verbes du premier groupe 3ème pers pluriel"],"ient_2":[{"+":"ent(s)?$"},"j",1,"#comment# si la règle précédente ne fonctionne pas"],"ill":[{"+":"ll","-":"[bcçdfghjklmnpqrstvwxz](u?)"},"i",1,"#comment# précédé éventuellement d un u et d une consonne, donne le son [i]"],"ing":[{"+":"ng$","-":"[bcçdfghjklmnpqrstvwxz]"},"i",1],"ll":[{"+":"ll"},"j",3,"#comment# par défaut avec ll donne le son [j]"],"lldeb":[{"+":"ll","-":"^"},"i",1],"m":[{"+":"m[bcçdfghjklnpqrstvwxz]"},"e~",2],"mill":[{"+":"ll","-":"m"},"i",1],"n":[{"+":"n[bcçdfghjklmpqrstvwxz]"},"e~",2],"nm":[{"+":"[n|m]$"},"e~",2],"prec_2cons":[{"-":"[ptkcbdgfv][lr]"},"i",1,"#comment# précédé de 2 consonnes (en position 3), doit apparaître comme [ij]"],"tranquille":[{"+":"ll","-":"tranqu"},"i",1],"ui":[{"+":"ent","-":"u"},"i",1,"#comment# essuient, appuient"],"vill":[{"+":"ll","-":"v"},"i",1]}],"j":[[],{"*":[{},"z^",1]}],"k":[[],{"*":[{},"k",1]}],"l":[["vill","mill","tranquille","illdeb","ill","eil","ll","excep_il","apostrophe","lisole"],{"*":[{},"l",1],"apostrophe":[{"+":"('|’)"},"l",2,"#comment# apostrophe"],"eil":[{"-":"e(u?)i"},"j",1,"#comment# les mots terminés en eil ou ueil => son [j]"],"excep_il":[{"+":"(s?)$","-":"fusi|outi|genti"},"#",1,"#comment# les exceptions trouvées ou le l à la fin ne se prononce pas : fusil, gentil, outil"],"ill":[{"+":"l","-":".i"},"j",2,"#comment# par défaut, ill donne le son [j]"],"illdeb":[{"+":"l","-":"^i"},"l",2,"#comment# ill en début de mot = son [l] ; exemple : illustration"],"lisole":[{"+":"$","-":"^"},"l",1,"#comment# exemple : l animal"],"ll":[{"+":"l"},"l",2,"#comment# à défaut de l application d une autre règle, ll donne le son [l]"],"mill":[{"+":"l","-":"^mi"},"l",2,"#comment# mille, million, etc. => son [l]"],"tranquille":[{"+":"l","-":"tranqui"},"l",2,"#comment# tranquille => son [l]"],"vill":[{"+":"l","-":"^vi"},"l",2,"#comment# ville, village etc. => son [l]"]}],"m":[["m","damn","tomn","misole","apostrophe"],{"*":[{},"m",1],"apostrophe":[{"+":"('|’)"},"m",2],"damn":[{"+":"n","-":"da"},"#",1,"#comment# regle spécifique pour damné et ses dérivés"],"m":[{"+":"m"},"m",2],"misole":[{"+":"$","-":"^"},"m",1,"#comment# exemple : m a"],"tomn":[{"+":"n","-":"to"},"#",1,"#comment# regle spécifique pour automne et ses dérivés"]}],"n":[["ing","n","ment","urent","irent","erent","ent","nisole","apostrophe"],{"*":[{},"n",1],"apostrophe":[{"+":"('|’)"},"n",2],"ent":[{"+":"t$","-":"e"},"verb_3p",2],"erent":[{"+":"t$","-":"ère"},"verb_3p",2,"#comment# verbes avec terminaisons en -èrent"],"ing":[{"+":"g$","-":"i"},"g~",2],"irent":[{"+":"t$","-":"ire"},"verb_3p",2,"#comment# verbes avec terminaisons en -irent"],"ment":["regle_verbe_mer","verb_3p",2,"#comment# on considère que les verbent terminés par -ment se prononcent [#]"],"n":[{"+":"n"},"n",2],"nisole":[{"+":"$","-":"^"},"n",1,"#comment# exemple : n a"],"urent":[{"+":"t$","-":"ure"},"verb_3p",2,"#comment# verbes avec terminaisons en -urent"]}],"o":[["in","oignon","i","tomn","monsieur","n","m","nm","y1","y2","u","o","oe_0","oe_1","oe_2","oe_3","voeux","oeufs","noeud","oeu_defaut","oe_defaut"],{"*":[{},"o",1],"i":[{"+":"(i|î)"},"wa",2],"in":[{"+":"i[nm]([bcçdfghjklnmpqrstvwxz]|$)"},"u",1],"m":[{"+":"m[bcçdfgjklpqrstvwxz]"},"o~",2,"#comment# toute consonne sauf le m"],"monsieur":[{"+":"nsieur","-":"m"},"q",2],"n":[{"+":"n[bcçdfgjklmpqrstvwxz]"},"o~",2],"nm":[{"+":"[nm]$"},"o~",2],"noeud":[{"+":"eud"},"x^",3,"#comment# noeud"],"o":[{"+":"o"},"o",2,"#comment# exemple : zoo"],"oe_0":[{"+":"ê"},"wa",2],"oe_1":[{"+":"e","-":"c"},"o",1,"#comment# exemple : coefficient"],"oe_2":[{"+":"e","-":"m"},"wa",2,"#comment# exemple : moelle"],"oe_3":[{"+":"e","-":"f"},"e",2,"#comment# exemple : foetus"],"oe_defaut":[{"+":"e"},"x",2,"#comment# exemple : oeil"],"oeu_defaut":[{"+":"eu"},"x",3,"#comment# exemple : oeuf"],"oeufs":[{"+":"eufs"},"x^",3,"#comment# traite oeufs et boeufs"],"oignon":[{"+":"ignon","-":"^"},"o",2],"tomn":[{"+":"mn","-":"t"},"o",1,"#comment# regle spécifique pour automne et ses dérivés"],"u":[{"+":"[uwûù]"},"u",2,"#comment# son [u] : clou, clown"],"voeux":[{"+":"eux"},"x^",3,"#comment# voeux"],"y1":[{"+":"y$"},"wa",2],"y2":[{"+":"y"},"wa",1]}],"p":[["h","oup","drap","trop","sculpt","sirop","sgalop","rps","amp","compt","bapti","sept","p"],{"*":[{},"p",1],"amp":[{"+":"$","-":"c(h?)am"},"#",1,"#comment# les exceptions avec un p muet en fin de mot : camp, champ"],"bapti":[{"+":"ti","-":"ba"},"#",1,"#comment# les exceptions avec un p muet : les mots en *bapti*"],"compt":[{"+":"t","-":"com"},"#",1,"#comment# les exceptions avec un p muet : les mots en *compt*"],"drap":[{"+":"$","-":"dra"},"#",1,"#comment# les exceptions avec un p muet en fin de mot : drap"],"h":[{"+":"h"},"f_ph",2],"oup":[{"+":"$","-":"[cl]ou"},"#",1,"#comment# les exceptions avec un p muet en fin de mot : loup, coup"],"p":[{"+":"p"},"p",2],"rps":[{"+":"s$","-":"[rm]"},"#",1,"#comment# les exceptions avec un p muet en fin de mot : corps, camp"],"sculpt":[{"+":"t","-":"scul"},"#",1,"#comment# les exceptions avec un p muet : sculpter et les mots de la même famille"],"sept":[{"+":"t(s?)$","-":"^se"},"#",1,"#comment# les exceptions avec un p muet en fin de mot : sept"],"sgalop":[{"+":"$","-":"[gs]alo"},"#",1,"#comment# les exceptions avec un p muet en fin de mot : galop"],"sirop":[{"+":"$","-":"siro"},"#",1,"#comment# les exceptions avec un p muet en fin de mot : sirop"],"trop":[{"+":"$","-":"tro"},"#",1,"#comment# les exceptions avec un p muet en fin de mot : trop"]}],"q":[["qu","k"],{"*":[{},"k",1],"k":[{"+":"u"},"k_qu",2],"qu":[{"+":"u[bcçdfgjklmnpqrstvwxz]"},"k",1]}],"r":[["monsieur","messieurs","gars","r"],{"*":[{},"r",1],"gars":[{"+":"s","-":"ga"},"#",2,"#comment# gars"],"messieurs":[{"-":"messieu"},"#",1],"monsieur":[{"-":"monsieu"},"#",1],"r":[{"+":"r"},"r",2]}],"s":[["sch","h","s_final","parasit","para","mars","s","z","sisole","smuet","apostrophe"],{"*":[{},"s",1],"@":[{},"#",1],"apostrophe":[{"+":"('|’)"},"s",2,"#comment# apostrophe"],"h":[{"+":"h"},"s^",2],"mars":[{"+":"$","-":"mar"},"s",1,"#comment# mars"],"para":[{"-":"para"},"s",1,"#comment# para quelque chose (parasol, parasismique, ...)"],"parasit":[{"+":"it","-":"para"},"z_s",1,"#comment# parasit*"],"s":[{"+":"s"},"s",2,"#comment# un s suivi d un autre s se prononce [s]"],"s_final":["regle_s_final","s",1,"#comment# quelques mots terminés par -us, -is, -os, -as"],"sch":[{"+":"ch"},"s^",3,"#comment# schlem"],"sisole":[{"+":"$","-":"^"},"s",1,"#comment# exemple : s approche"],"smuet":[{"+":"$","-":"(e?)"},"#",1,"#comment# un s en fin de mot éventuellement précédé d un e ex. : correctes"],"z":[{"+":"[aeiyouéèàüûùëöêîô]","-":"[aeiyouéèàüûùëöêîô]"},"z_s",1,"#comment# un s entre 2 voyelles se prononce [z]"]}],"t":[["t","tisole","except_tien","_tien","cratie","vingt","tion","ourt","_inct","_spect","_ct","_est","t_final","tmuet","apostrophe"],{"*":[{},"t",1],"@":[{},"#",1],"_ct":[{"+":"(s?)$","-":"c"},"t",1,"#comment# tous les autres mots terminés par -ct"],"_est":[{"+":"(s?)$","-":"es"},"t",1,"#comment# test, ouest, brest, west, zest, lest"],"_inct":[{"+":"(s?)$","-":"inc"},"#",1,"#comment# instinct, succinct, distinct"],"_spect":[{"+":"(s?)$","-":"spec"},"#",1,"#comment# respect, suspect, aspect"],"_tien":[{"+":"ien"},"s_t",1],"apostrophe":[{"+":"('|’)"},"t",2,"#comment# apostrophe"],"cratie":[{"+":"ie","-":"cra"},"s_t",1],"except_tien":["regle_tien","t",1,"#comment# quelques mots où tien se prononce [t]"],"ourt":[{"+":"$","-":"(a|h|g)our"},"t",1,"#comment# exemple : yaourt, yoghourt, yogourt"],"t":[{"+":"t"},"t",2],"t_final":["regle_t_final","t",1,"#comment# quelques mots où le t final se prononce"],"tion":[{"+":"ion"},"s_t",1],"tisole":[{"+":"$","-":"^"},"t",1,"#comment# exemple : demande-t-il"],"tmuet":[{"+":"(s?)$"},"#",1,"#comment# un t suivi éventuellement d un s ex. : marrants"],"vingt":[{"+":"$","-":"ving"},"t",1,"#comment# vingt mais pas vingts"]}],"u":[["um","n","nm","ueil"],{"*":[{},"y",1],"n":[{"+":"n[bcçdfghjklmpqrstvwxz]"},"x~",2],"nm":[{"+":"[nm]$"},"x~",2],"ueil":[{"+":"eil"},"x",2,"#comment# mots terminés en ueil => son [x^]"],"um":[{"+":"m$","-":"[^aefo]"},"o",1]}],"v":[[],{"*":[{},"v",1]}],"w":[["wurt","wisig","wag","wa","wi"],{"*":[{},"w",1],"wa":[{"+":"a"},"wa",2,"#comment# watt, wapiti, etc."],"wag":[{"+":"ag"},"v",1,"#comment# wagons et wagnérien"],"wi":[{"+":"i"},"u",1,"#comment# kiwi"],"wisig":[{"+":"isig"},"v",1,"#comment# wisigoth"],"wurt":[{"+":"urt"},"v",1,"#comment# saucisse"]}],"x":[["six_dix","gz_1","gz_2","gz_3","gz_4","gz_5","_aeox","fix","_ix"],{"*":[{},"ks",1],"@":[{},"#",1],"_aeox":[{"-":"[aeo]"},"ks",1],"_ix":[{"-":"(remi|obéli|astéri|héli|phéni|féli)"},"ks",1],"fix":[{"-":"fi"},"ks",1],"gz_1":[{"+":"[aeiouéèàüëöêîôûù]","-":"^"},"gz",1,"#comment# mots qui commencent par un x suivi d une voyelle"],"gz_2":[{"+":"[aeiouéèàüëöêîôûù]","-":"^(h?)e"},"gz",1,"#comment# mots qui commencent par un ex ou hex suivi d une voyelle"],"gz_3":[{"+":"[aeiouéèàüëöêîôûù]","-":"^coe"},"gz",1,"#comment# mots qui commencent par un coex suivi d une voyelle"],"gz_4":[{"+":"[aeiouéèàüëöêîôûù]","-":"^ine"},"gz",1,"#comment# mots qui commencent par un inex suivi d une voyelle"],"gz_5":[{"+":"[aeiouéèàüëöêîôûù]","-":"^(p?)rée"},"gz",1,"#comment# mots qui commencent par un réex ou préex suivi d une voyelle"],"six_dix":[{"-":"(s|d)i"},"s_x",1]}],"y":[["m","n","nm","abbaye","y_voyelle"],{"*":[{},"i",1],"abbaye":[{"+":"e","-":"abba"},"i",1,"#comment# abbaye... bien irrégulier"],"m":[{"+":"m[mpb]"},"e~",2],"n":[{"+":"n[bcçdfghjklmpqrstvwxz]"},"e~",2],"nm":[{"+":"[n|m]$"},"e~",2],"y_voyelle":[{"+":"[aeiouéèàüëöêîôûù]"},"j",1,"#comment# y suivi d une voyelle donne [j]"]}],"z":[["raz_riz"],{"*":[{},"z",1],"@":[{},"z",1],"raz_riz":[{"+":"$","-":"^r[ai]"},"#",1,"#comment# raz et riz : z = #"]}],"à":[[],{"*":[{},"a",1]}],"â":[[],{"*":[{},"a",1]}],"ç":[[],{"*":[{},"s",1]}],"è":[[],{"*":[{},"e^",1]}],"é":[[],{"*":[{},"e",1]}],"ê":[[],{"*":[{},"e^",1]}],"ë":[[],{"*":[{},"e^",1]}],"î":[[],{"*":[{},"i",1]}],"ï":[["thai","aie"],{"*":[{},"i",1],"aie":[{"+":"e","-":"[ao]"},"j",1,"#comment# païen et autres"],"thai":[{"-":"t(h?)a"},"j",1,"#comment# taï, thaï et dérivés"]}],"ô":[[],{"*":[{},"o",1]}],"ö":[[],{"*":[{},"o",1]}],"ù":[[],{"*":[{},"y",1]}],"û":[[],{"*":[{},"y",1]}],"œ":[["voeux","oeufs","noeud"],{"*":[{"+":"u"},"x^",2],"noeud":[{"+":"ud"},"x^",2,"#comment# noeud"],"oeufs":[{"+":"ufs"},"x^",2,"#comment# traite oeufs et boeufs"],"voeux":[{"+":"ux"},"x^",2,"#comment# voeux"]}]}
""")

###################################################################################
# Élimine des caractères de la chaîne de caractères à traiter
###################################################################################
def nettoyeur_caracteres(paragraphe):
    # suppression des \r qui engendrent des décalages de codage sous W*
    nparagraphe = paragraphe.replace('\r', '')

    return nparagraphe


###################################################################################
# Élimine les caractères accentués et les remplace par des non accentués
###################################################################################
def texte_sans_accent(texte):
    ultexte = texte.lower()  # tout mettre en minuscules
    ultexte = re.sub('[àäâ]', 'a', ultexte)
    ultexte = re.sub('[éèêë]', 'e', ultexte)
    ultexte = re.sub('[îï]', 'i', ultexte)
    ultexte = re.sub('[ôö]', 'o', ultexte)
    ultexte = re.sub('[ûù]', 'u', ultexte)
    ultexte = re.sub('ç', 'c', ultexte)
    ultexte = re.sub('œ', 'e', ultexte)

    return ultexte


###################################################################################
# Élimine des caractères de la chaîne de caractères à traiter
###################################################################################
def pretraitement_texte(texte, substitut=' '):
    ultexte = texte.lower()  # tout mettre en minuscules
    ultexte = re.sub('[\'´’]', '@', ultexte)  # remplace les apostrophes par des @
    ultexte = re.sub('[^a-zA-Z0-9@àäâéèêëîïôöûùçœ]', substitut, ultexte)  # ne garde que les caractères significatifs

    return ultexte


###################################################################################
# Teste l'application d'une règle
###################################################################################
def teste_regle(nom_regle, cle, mot, pos_mot):

    logging.debug ('mot : ' + mot + '[' + str(pos_mot - 1) + '] lettre : ' + mot[pos_mot - 1] + ' regle : ' + nom_regle)
    if not isinstance(cle, dict):
        # la regle est une fonction spécifique
        # logging.debug(nom_regle, ' fonction');
        return globals()[cle](mot, pos_mot)

    # exemples : '+':'n|m' ou '-':'[aeiou]'
    trouve_s = True
    trouve_p = True

    if '+' in cle.keys():
        logging.debug(nom_regle + ' cle + testee : ' + cle['+'])
        logging.debug (mot, pos_mot)
        # il faut lire les lettres qui suivent
        # recherche le modèle demandé au début de la suite du mot
        pattern = re.compile(cle['+'])
        res = pattern.match(mot, pos_mot)
        trouve_s = ((res != None) and (res.start() == pos_mot))
    
    if '-' in cle.keys():
        logging.debug(nom_regle + ' cle - testee : ' + cle['-']);
        trouve_p = False
        pattern = re.compile(cle['-'])
        # teste si la condition inclut le début du mot ou seulement les lettres qui précèdent
        if (cle['-'][0] == '^'):
            # le ^ signifie 'début de chaîne' et non 'tout sauf'
            if (len(cle['-']) == 1):
                # on vérifie que le début de mot est vide
                trouve_p = (pos_mot == 1)
            else:
                # le début du mot doit correspondre au pattern
                res = pattern.match(mot, 0, pos_mot)
                if (res != None):
                    trouve_p = (res.end() - res.start() + 1 == pos_mot)
        else :
            k = pos_mot - 2
            while ((k > -1) and (not trouve_p)):
                logging.debug (mot, k, pos_mot)
                # il faut lire les lettres qui précèdent
                # recherche le modèle demandé à la fin du début du mot
                res = pattern.match(mot, k, pos_mot)
                if (res != None):
                    # print (res.end(), res.start())
                    trouve_p = (res.end() - res.start() + 1 == pos_mot - k)
                k -= 1

    return (trouve_p and trouve_s)


###################################################################################
# Décodage d'un mot sous la forme d'une suite de phonèmes
###################################################################################
def extraire_phonemes(mot, para=None, p_para=0, detection_phonemes_debutant=0, mode=ConstLireCouleur.SYLLABES_ECRITES):
    p_mot = 0
    codage = []
    if para is None:
        para = mot
        
    lcdico = LCDictionnary()

    logging.info('--------------------' + mot + '--------------------')
    if mot in lcdico.getKeys():
        entree = lcdico.getEntry(mot)[0].strip()
        if len(entree) > 0:
            """ Le mot est dans le dictionnaire et le décodage doit être fait en conséquence """
            smot = re.split('/', entree)
            i = 0
            while p_mot < len(mot) and i < len(smot):
                lsmot = smot[i].split('.')  # séparer graphème effectif et graphème correspondant au phonème souhaité
                phon = lsmot[0]
                if len(lsmot) > 1 and len(lsmot[1]) > 0:
                    phon = lsmot[1]
                try:
                    # est-ce que le phonème est codé en direct ?
                    phoneme = re.findall('\[(.*)\]', phon)
                    if len(phoneme) > 0:
                        # oui
                        try:
                            codage.append((sampa2lc[phoneme[0]], lsmot[0]))
                        except:
                            codage.append((phoneme[0], lsmot[0]))
                    else:
                        # non : on le décode à partir des lettres
                        phoneme = extraire_phonemes(phon, None, 0, detection_phonemes_debutant)
                        if len(phoneme[0][0]) > 0:
                            codage.append((phoneme[0][0], lsmot[0]))
                        else:
                            codage.append((phon, lsmot[0]))

                except:
                    codage.append((phon, lsmot[0]))
                p_mot += len(lsmot[0])
                i += 1

    """ Le mot n'est dans le dictionnaire et le décodage est standard """
    while p_mot < len(mot):
        # On teste d'application des règles de composition des sons
        lettre = mot[p_mot]
        logging.debug ('lettre : ' + lettre)

        trouve = False
        if lettre in autom:
            aut = autom[lettre][1]
            logging.debug ('liste des règles : ' + str(aut))
            i = 0
            while (not trouve) and (i < len(autom[lettre][0])):
                k = autom[lettre][0][i]
                if teste_regle(k, aut[k][0], mot, p_mot + 1):
                    phoneme = aut[k][1]
                    pas = aut[k][2]
                    codage.append((phoneme, para[p_para:p_para + pas]))
                    logging.info('phoneme:' + phoneme + ' ; lettre(s) lue(s):' + para[p_para:p_para + pas])
                    p_mot += pas
                    p_para += pas
                    trouve = True
                i += 1
            logging.debug ('trouve:' + str(trouve) + ' - ' + str(codage))

            if (not trouve) and (p_mot == len(mot) - 1) and ('@' in aut):
                if p_mot == len(mot) - 1:
                    # c'est la dernière lettre du mot, il faut vérifier que ce n'est pas une lettre muette
                    phoneme = aut['@'][1]
                    pas = 1
                    codage.append((phoneme, lettre))
                    trouve = True
                    p_mot += 1
                    p_para += 1
                    logging.info('phoneme fin de mot:' + phoneme + ' ; lettre lue:' + lettre)

            # rien trouvé donc on prend le phonème de base ('*')
            if not trouve:
                try:
                    phoneme = aut['*'][1]
                    pas = aut['*'][2]
                    codage.append((phoneme, para[p_para:p_para + pas]))
                    p_para += pas
                    p_mot += pas
                    logging.info('phoneme par defaut:' + phoneme + ' ; lettre lue:' + lettre)
                except:
                    codage.append(('', lettre))
                    p_para += 1
                    p_mot += 1
                    logging.info('non phoneme ; caractere lu:' + lettre)
        else:
            codage.append(('', lettre))
            p_mot += 1
            p_para += 1
            logging.info('non phoneme ; caractere lu:' + lettre)

    logging.info('--------------------' + str(codage) + '--------------------')

    # post traitement pour différencier les eu ouverts et les eu fermés
    codage = post_traitement_e_ouvert_ferme(codage)

    if not detection_phonemes_debutant:
        # post traitement pour associer u + [an, in, en, on, a, é, etc.]
        codage = post_traitement_w(codage)

        # post traitement pour associer yod + [an, in, en, on, a, é, etc.]
        codage = post_traitement_yod(codage, mode)

        # post traitement pour différencier les o ouverts et les o fermés
        codage = post_traitement_o_ouvert_ferme(codage)

    return codage


def all_indices(value, qlist):
    indices = []
    if isinstance(value, list):
        for v in value:
            idx = -1
            while True:
                try:
                    idx = qlist.index(v, idx + 1)
                    indices.append(idx)
                except ValueError:
                    break

    else:
        idx = -1
        while True:
            try:
                idx = qlist.index(value, idx + 1)
                indices.append(idx)
            except ValueError:
                break
    return indices


###################################################################################
# Post traitement la constitution d'allophones des phonèmes avec yod
# référence : voir http://andre.thibault.pagesperso-orange.fr/PhonologieSemaine10.pdf (cours du 3 février 2016)
###################################################################################
def post_traitement_yod(pp, mode=ConstLireCouleur.SYLLABES_ECRITES):
    if not isinstance(pp, list) or len(pp) == 1:
        return pp

    phon_suivant = ['a', 'a~', 'e', 'e^', 'e_comp', 'e^_comp', 'o', 'o_comp', 'o~', 'e~', 'q', 'x', 'x^', 'u']
    if mode == ConstLireCouleur.SYLLABES_ECRITES:
        phon_suivant.append('q_caduc')

    phonemes = [x[0] for x in pp]

    # recherche de tous les indices de phonèmes avec 'i' ou 'j'
    i_j = all_indices(['i', 'j'], phonemes[:-1])
    i_j.reverse() # commencer par la fin puisqu'on risque de compacter la chaîne de phonèmes
    for i_ph in i_j:
        # phonème suivant
        if phonemes[i_ph + 1] in phon_suivant:
            pp[i_ph] = ('j_' + phonemes[i_ph + 1], pp[i_ph][1] + pp[i_ph + 1][1])
            if len(pp[i_ph + 2:]) > 0:
                pp[i_ph + 1:] = pp[i_ph + 2:]  # compactage de la chaîne de phonèmes
            else:
                del pp[-1]

    return pp


###################################################################################
# Post traitement la constitution d'allophones des phonèmes avec w
# référence : voir http://andre.thibault.pagesperso-orange.fr/PhonologieSemaine10.pdf (cours du 3 février 2016)
###################################################################################
def post_traitement_w(pp):
    if not isinstance(pp, list) or len(pp) == 1:
        return pp

    phonemes = [x[0] for x in pp]
    
    # transformation des [wa] en [w_a]
    i_j = all_indices('wa', phonemes)
    for i_ph in i_j:
        pp[i_ph] = ('w_a', pp[i_ph][1])
    
    # recherche de tous les indices de phonèmes avec 'u'
    i_j = all_indices('u', phonemes[:-1])
    i_j.reverse() # commencer par la fin puisqu'on risque de compacter la chaîne de phonèmes
    for i_ph in i_j:
        # phonème suivant
        phon_suivant = ['a', 'a~', 'e', 'e^', 'e_comp', 'e^_comp', 'o', 'o_comp', 'o~', 'e~', 'x', 'x^', 'i']
        if phonemes[i_ph + 1] in phon_suivant:
            pp[i_ph] = ('w_' + phonemes[i_ph + 1], pp[i_ph][1] + pp[i_ph + 1][1])
            if len(pp[i_ph + 2:]) > 0:
                pp[i_ph + 1:] = pp[i_ph + 2:]  # compactage de la chaîne de phonèmes
            else:
                del pp[-1]

    return pp


###################################################################################
# Post traitement pour déterminer si le son [o] est ouvert ou fermé
###################################################################################
def post_traitement_o_ouvert_ferme(pp):
    if not isinstance(pp, list) or len(pp) == 1:
        return pp

    phonemes = [x[0] for x in pp]
    if not 'o' in phonemes:
        # pas de 'o' dans le mot
        return pp

    # consonnes qui rendent possible un o ouvert en fin de mot
    consonnes_syllabe_fermee = ['p', 'k', 'b', 'd', 'g', 'f', 'f_ph', 's^', 'l', 'r', 'm', 'n']

    # mots en 'osse' qui se prononcent avec un o ouvert
    mots_osse = json.loads("""
    ["cabosse", "carabosse", "carrosse", "colosse", "molosse", "cosse", "crosse", "bosse", "brosse", "rhinocéros", "désosse", "fosse", "gosse", "molosse", "écosse", "rosse", "panosse"]
    """)

    # indice du dernier phonème prononcé
    nb_ph = len(phonemes) - 1
    while nb_ph > 0 and phonemes[nb_ph] == "#":
        nb_ph -= 1

    # recherche de tous les indices de phonèmes avec 'o'
    i_o = all_indices('o', phonemes[:nb_ph + 1])

    # reconstitution du mot sans les phonèmes muets à la fin
    mot = ''.join([x[1] for x in pp[:nb_ph + 1]])

    if mot in mots_osse:
        # certains mots en 'osse' on un o ouvert
        i_ph_o = i_o[-1:][0]
        pp[i_ph_o] = ('o_ouvert', pp[i_ph_o][1])
        return pp

    for i_ph in i_o:
        if i_ph == nb_ph:
            # syllabe tonique ouverte (rien après ou phonème muet) en fin de mot : o fermé
            return pp

        if pp[i_ph][1] != 'ô':
            if i_ph == nb_ph - 2 and phonemes[i_ph + 1] in consonnes_syllabe_fermee and phonemes[i_ph + 2] == 'q_caduc':
                # syllabe tonique fermée (présence de consonne après) en fin de mot : o ouvert
                pp[i_ph] = ('o_ouvert', pp[i_ph][1])
            elif phonemes[i_ph + 1] in ['r', 'z^_g', 'v']:
                # o ouvert lorsqu’il est suivi d’un [r] : or, cor, encore, dort, accord
                # o ouvert lorsqu’il est suivi d’un [z^_g] : loge, éloge, horloge
                # o ouvert lorsqu’il est suivi d’un [v] : ove, innove.
                pp[i_ph] = ('o_ouvert', pp[i_ph][1])
            elif (i_ph < nb_ph - 2) and (phonemes[i_ph + 1] in syllaphon['c']) and (phonemes[i_ph + 2] in syllaphon['c']):
                # un o suivi de 2 consonnes est un o ouvert
                pp[i_ph] = ('o_ouvert', pp[i_ph][1])

    return pp


###################################################################################
# Post traitement pour déterminer si le son [e] est ouvert "e" ou fermé "eu"
###################################################################################
def post_traitement_e_ouvert_ferme(pp):
    if not isinstance(pp, list) or len(pp) == 1:
        return pp

    phonemes = [x[0] for x in pp]
    if not 'x' in phonemes:
        # pas de 'eu' dans le mot
        return pp

    # indice du dernier phonème prononcé
    nb_ph = len(phonemes) - 1
    while nb_ph >= 1 and phonemes[nb_ph] == "#":
        nb_ph -= 1

    # recherche de tous les indices de phonèmes avec 'x' qui précèdent le dernier phonème prononcé
    i_x = all_indices('x', phonemes[:nb_ph + 1])

    # on ne s'intéresse qu'au dernier phonème (pour les autres, on ne peut rien décider)
    i_ph = i_x[-1]

    if i_ph < nb_ph - 2:
        # le phonème n'est pas l'un des 3 derniers du mot : on ne peut rien décider
        return pp

    if i_ph == nb_ph:
        # le dernier phonème prononcé dans le mot est le 'eu' donc 'eu' fermé
        pp[i_ph] = ('x^', pp[i_ph][1])
        return pp

    # le phonème est l'avant dernier du mot (syllabe fermée)
    consonnes_son_eu_ferme = ['z', 'z_s', 't']
    if phonemes[i_ph + 1] in consonnes_son_eu_ferme and phonemes[nb_ph] == 'q_caduc':
        pp[i_ph] = ('x^', pp[i_ph][1])

    return pp


###################################################################################
# Recomposition des phonèmes en une suite de syllabes (utilitaire)
###################################################################################
def extraire_syllabes_util(phonemes, mode=(ConstLireCouleur.SYLLABES_LC, ConstLireCouleur.SYLLABES_ECRITES)):
    nb_phon = len(phonemes)
    if nb_phon < 2:
        return [range(nb_phon)], phonemes

    lcdico = LCDictionnary()

    mot = ''.join([phonemes[i][1] for i in range(len(phonemes))])
    if mot in lcdico.getKeys():
        entree = lcdico.getEntry(mot)[1].strip()
        if len(entree) > 0:
            """ Le mot est dans le dictionnaire et le décodage doit être fait en conséquence """
            smot = re.split('/', entree)
            if ''.join(smot) == mot:  # ultime vérification
                j = 0
                i = 0
                sylls = []
                while i < len(smot):
                    cur_syl = []
                    csyl = ''
                    while j < len(phonemes) and csyl != smot[i]:
                        cur_syl.append(j)
                        csyl = ''.join([phonemes[cur_syl[k]][1] for k in range(len(cur_syl))])
                        j += 1
                    # ajouter la syllabe à la liste
                    sylls.append(cur_syl)
                    i += 1

                logging.info('--------------------' + str(sylls) + '--------------------')
                return sylls, [ph for ph in phonemes]

    """ Le mot n'est dans le dictionnaire et le décodage est standard """
    nphonemes = []
    if mode[0] == ConstLireCouleur.SYLLABES_STD:
        # dupliquer les phonèmes qui comportent des consonnes doubles
        for i in range(nb_phon):
            phon = phonemes[i]
            if isinstance(phon, tuple):
                if (phon[0] in syllaphon['c'] or phon[0] in syllaphon['s']) and (len(phon[1]) > 1):
                    if phon[1][-1] == phon[1][-2]:
                        # consonne redoublée ou plus
                        nphonemes.append((phon[0], phon[1][:-1]))
                        nphonemes.append((phon[0], phon[1][-1]))
                    else:
                        nphonemes.append(phon)
                else:
                    nphonemes.append(phon)
            else:
                nphonemes.append(phon)
    else:
        nphonemes = [ph for ph in phonemes]
    nb_phon = len(nphonemes)

    logging.info('--------------------' + str(nphonemes) + '--------------------')
    # préparer la liste de syllabes
    sylph = []
    for i in range(nb_phon):
        phon = nphonemes[i]
        if isinstance(phon, tuple):
            if phon[0] in syllaphon['v']:
                sylph.append(('v', [i]))
            elif phon[0].startswith('j_') or phon[0].startswith('w_') or phon[0].startswith('y_'):  # yod+voyelle, 'w'+voyelle, 'y'+voyelle sans diérèse
                sylph.append(('v', [i]))
            elif phon[0] in syllaphon['c']:
                sylph.append(('c', [i]))
            elif phon[0] in syllaphon['s']:
                sylph.append(('s', [i]))
            else:
                # c'est un phonème muet : '#'
                sylph.append(('#', [i]))

    # mixer les doubles phonèmes de consonnes qui incluent [l] et [r] ; ex. : bl, tr, cr, chr, pl
    i = 0
    while i < len(sylph) - 1:
        if ((sylph[i][0] == 'c') and (sylph[i + 1][0] == 'c')):
            # deux phonèmes consonnes se suivent
            phon0 = nphonemes[sylph[i][1][0]]
            phon1 = nphonemes[sylph[i + 1][1][0]]
            if ((phon1[0] == 'l') or (phon1[0] == 'r')) and (phon0[0] in ['b', 'k', 'p', 't', 'g', 'd', 'f', 'v']):
                # mixer les deux phonèmes puis raccourcir la chaîne
                sylph[i][1].extend(sylph[i + 1][1])
                for j in range(i + 1, len(sylph) - 1):
                    sylph[j] = sylph[j + 1]
                sylph.pop()
        i += 1
    logging.info("mixer doubles phonèmes consonnes (bl, tr, cr, etc.) :" + str(sylph))

    # mixer les doubles phonèmes [y] et [i], [u] et [i,e~,o~]
    i = 0
    while i < len(sylph) - 1:
        if ((sylph[i][0] == 'v') and (sylph[i + 1][0] == 'v')):
            # deux phonèmes voyelles se suivent
            phon1 = nphonemes[sylph[i][1][0]][0]
            phon2 = nphonemes[sylph[i + 1][1][0]][0]
            if (phon1 == 'y' and phon2 == 'i') or (phon1 == 'u' and phon2 in ['i', 'e~', 'o~']):
                # mixer les deux phonèmes puis raccourcir la chaîne
                sylph[i][1].extend(sylph[i + 1][1])
                for j in range(i + 1, len(sylph) - 1):
                    sylph[j] = sylph[j + 1]
                sylph.pop()
        i += 1
    logging.info("mixer doubles phonèmes voyelles ([y] et [i], [u] et [i,e~,o~]) :" + str(sylph))

    # accrocher les lettres muettes aux lettres qui précèdent
    i = 0
    while i < len(sylph) - 1:
        if sylph[i + 1][0] == '#':
            # mixer les deux phonèmes puis raccourcir la chaîne
            sylph[i][1].extend(sylph[i + 1][1])
            for j in range(i + 1, len(sylph) - 1):
                sylph[j] = sylph[j + 1]
            sylph.pop()
        i += 1

    # construire les syllabes par association de phonèmes consonnes et voyelles
    sylls = []
    nb_sylph = len(sylph)
    i = j = 0
    while i < nb_sylph:
        # début de syllabe = tout ce qui n'est pas voyelle
        j = i
        while (i < nb_sylph) and (sylph[i][0] != 'v'):
            i += 1

        # inclure les voyelles
        if (i < nb_sylph) and (sylph[i][0] == 'v'):
            i += 1
            cur_syl = []
            for k in range(j, i):
                cur_syl.extend(sylph[k][1])
            j = i

            # ajouter la syllabe à la liste
            sylls.append(cur_syl)

        # la lettre qui suit est une consonne
        if i + 1 < nb_sylph:
            lettre1 = nphonemes[sylph[i][1][-1]][1][-1]
            lettre2 = nphonemes[sylph[i + 1][1][0]][1][0]
            if 'bcdfghjklmnpqrstvwxzç'.find(lettre1) > -1 and 'bcdfghjklmnpqrstvwxzç'.find(lettre2) > -1:
                # inclure cette consonne si elle est suivie d'une autre consonne
                cur_syl.extend(sylph[i][1])
                i += 1
                j = i

    # précaution de base : si pas de syllabes reconnues, on concatène simplement les phonèmes
    if len(sylls) == 0:
        return [range(nb_phon)], phonemes

    # il ne doit rester à la fin que les lettres muettes ou des consonnes qu'on ajoute à la dernière syllabe
    for k in range(j, nb_sylph):
        sylls[-1].extend(sylph[k][1])

    if mode[1] == ConstLireCouleur.SYLLABES_ORALES and len(sylls) > 1:
        # syllabes orales : si la dernière syllabe est finalisée par des lettres muettes ou un e caduc,
        # il faut la concaténer avec la syllabe précédente
        k = len(sylls[-1]) - 1
        while k > 0 and nphonemes[sylls[-1][k]][0] in ['#', 'verb_3p']:
            k -= 1
        if nphonemes[sylls[-1][k]][0] .endswith('q_caduc'):
            # concaténer la dernière syllabe à l'avant-dernière
            sylls[-2].extend(sylls[-1])
            del sylls[-1]

    # ménage
    del sylph

    return sylls, nphonemes


###################################################################################
# Recomposition des phonèmes en une suite de syllabes
###################################################################################
def extraire_syllabes(phonemes, mode=(ConstLireCouleur.SYLLABES_LC, ConstLireCouleur.SYLLABES_ECRITES)):
    sylls, nphonemes = extraire_syllabes_util(phonemes, mode)

    # recomposer les syllabes avec les lettres
    try:
        lsylls = [''.join([nphonemes[i][1] for i in sylls[j]]) for j in range(len(sylls))]
    except:
        lsylls = [''.join([phonemes[i][1] for i in range(len(phonemes))])]

    # ménage
    del sylls
    del nphonemes

    logging.info('--------------------' + str(lsylls) + '--------------------')
    return lsylls


###################################################################################
# Recherche une succession de phonèmes dans le mot traduit sous la forme de phonèmes
###################################################################################
def generer_masque_phonemes(phonemes, l_phon):
    mask = []
    if type(phonemes) == type(list):
        return mask

    i = 0
    nb_phon = len(phonemes)
    nb_l_phon = len(l_phon)
    while i < nb_phon:
        phon = phonemes[i][0].split('_')[0]
        if phon == l_phon[0]:
            j = 1
            k = i + 1
            trouve = True
            while (k < nb_phon) and trouve and (j < nb_l_phon):
                phon = phonemes[k][0].split('_')[0]
                trouve = (phon == l_phon[j])
                j += 1
                k += 1
            if trouve and (j == nb_l_phon):
                # on est arrivé à la fin du pattern
                mask.extend([1 for u in range(nb_l_phon)])
                i += nb_l_phon
            else:
                # on n'a pas trouvé le pattern complet
                mask.append(0)
                i += 1
        else:
            mask.append(0)
            i += 1
    return mask


###################################################################################
# Recompose les phonèmes des mots d'un paragraphe en syllabes
###################################################################################
def generer_paragraphe_syllabes(pp, mode=(ConstLireCouleur.SYLLABES_LC, ConstLireCouleur.SYLLABES_ECRITES)):
    ps = []
    for umot in pp:
        if isinstance(umot, list):
            ps.append(extraire_syllabes(umot, mode))
        else:
            ps.append(umot)
    return ps


###################################################################################
# Recherche la succession de phonèmes demandée dans les mots du paragraphe
###################################################################################
def generer_masque_paragraphe_phonemes(pp, l_phon):
    pm = []
    for umot in pp:
        if isinstance(umot, list):
            pm.append(generer_masque_phonemes(umot, l_phon))
        else:
            pm.append([])
    return pm


###################################################################################
# Générateur d'un paragraphe de texte sous la forme de phonèmes
###################################################################################
def generer_paragraphe_phonemes(texte, detection_phonemes_debutant=0, mode=ConstLireCouleur.SYLLABES_ECRITES):
    """ Transformation d'un paragraphe en une liste de phonèmes """

    # #
    # Prétraitement
    # #
    ultexte = pretraitement_texte(texte)
    l_utexte = len(ultexte)
    mots = ultexte.split()  # extraire les mots

    # #
    # Traitement
    # #
    pp = []
    p_texte = 0
    for umot in mots:
        # recherche de l'emplacement du nouveau mot à traiter
        pp_texte = ultexte.find(umot, p_texte)
        if pp_texte > p_texte:
            # ajoute au paragraphe la portion de texte non traitée (ponctuation, espaces...)
            pp.append(texte[p_texte:pp_texte])
        p_texte = pp_texte

        # décodage du mot en phonèmes
        phonemes = extraire_phonemes(umot, texte, p_texte, detection_phonemes_debutant, mode)
        pp.append(phonemes)
        p_texte += len(umot)

    # ajouter le texte qui suit le dernier mot
    if p_texte < l_utexte:
        pp.append(texte[p_texte:])

    return pp


###################################################################################
# Teste s'il est ou non possible de faire une liaison vers ce mot
###################################################################################
def debut_mot_h_muet(mot):
    mots_h_muet = ['habilet.', 'habill.', 'habit.', 'haleine', 'hallucination',
    'halt.re', 'hebdomadaire', 'hame.on', 'harmonie', 'héberge', 'hébétude', 'hécatombe',
    'hégémonie', 'hémi.', 'héritage', 'herbe', 'héréditaire', 'hermine',
    'hermétique', 'héroïne', 'hésit.', 'heure', 'hippopotame', 'hiver', 'histoire',
    'homme', 'hommage', 'homonyme', 'honn[ê|e]te', 'honneur', 'h[ô|o]tel', 'h[ô|o]pita',
    'horizon', 'horloge', 'horoscope', 'horreur', 'horripil.', 'horticulteur', 'hospice',
    'hostilité', 'huile', 'huissier', 'hu[î|i]tre', 'humanité', 'humble',
    'humect.', 'humeur', 'humidité', 'humus', 'humili.', 'hymne']

    for pattern in mots_h_muet:
        if re.match(pattern, mot):
            return True
    return False


###################################################################################
# Teste s'il est ou non possible de faire une liaison vers ce mot
###################################################################################
def teste_liaison_vers_mot(mot):
    liaison_interdite = ['et', 'ou', 'onze', 'onzes', 'huit', 'huits']
    voyelles = '^[aeiouyùüïîöôàèéêë]'

    if mot == "est-ce":
        return True
    if mot.find('-') >= 0:  # risque d'inversion sujet verbe et liaison interdite ensuite
        return False
    if re.match(voyelles, mot[:1]) and not mot in liaison_interdite:
        return True
    return debut_mot_h_muet(mot)


###################################################################################
# Teste s'il y a ou non une liaison entre les 2 mots
###################################################################################
def teste_liaison(mot_prec, mot_suiv):
    determinants = ['un', 'qu@un', 'des', 'les', 'cet', 'ces', 'mon', 'ton', 'son', 'mes', 'tes',
    'ses', 'nos', 'vos', 'leurs', 'aux', 'aucun', 'tout', 'quels', 'quelles', 'quelques',
    'deux', 'trois', 'cinq', 'six', 'sept', 'huit', 'neuf', 'dix', 'vingt', 'cent',
    'cents', 'quel', 'quels', 'quelles', 'aucun', 'certains', 'quelques', 'toutes', 'aux']
    pronoms = ['on', 'nous', 'vous', 'ils', 'elles', 'on', 'chacun', 'autres', 'tous', 'nul',
    'certains', 'certaines', 'dont']
    verbe_etre = ['suis', 'es', 'est', 'sommes', 'êtes', 'sont', 'étais', 'était', 'étaient',
    'étions', 'étiez', 'seras', 'serez', 'serons', 'seront', 'serais',
    'seriez', 'serions', 'fus', 'fut', 'fûmes', 'fûtes', 'furent', 'c@était', 'c@est']
    adjectifs_anteposes = ['petit', 'petits', 'grand', 'grands', 'beaux', 'bel', 'belles', 'bon', 'bons', 'mal',
    'autres', 'braves', 'gros', 'grosses', 'jeunes', 'jolis', 'mauvais', 'mauvaises', 'mêmes',
    'meilleur', 'meilleurs', 'meilleures', 'moindres', 'pires', 'vieux', 'vieil', 'vieilles']
    adverbes = ['après', 'hier', 'puis', 'soudain', 'tôt', 'tard', 'toujours', 'souvent',
    'trop', 'très', 'moins', 'plus', 'tant', 'tout', 'assez', 'bien', 'jamais', 'mieux', 'autant',
    'beaucoup']
    prepositions = ['par', 'pour', 'sans', 'avec', 'dans', 'sur', 'chez', 'durant', 'en', 'sous']
    divers = ['ont', 'faut', 'quant', 'quand', 'mais', 'donc', 'car']

    liaison_obligatoire = []
    for x in [determinants, pronoms, verbe_etre, adjectifs_anteposes, adverbes, prepositions, divers]:
        liaison_obligatoire.extend(x)

    liaison_interdite = ['tiens', 'pars', 'viens', 'vends', 'lis', 'dis', 'ris', 'perds', 'tonds', 'mets', 'allés']

    if teste_liaison_vers_mot(mot_suiv):
        # mode aide où certaines liaisons obligatoires sont marquées
        if mot_prec in liaison_obligatoire:
            return True
        if mot_prec in verbe_etre:
            return True
        if mot_prec[-1] == 's' and not mot_prec in liaison_interdite and debut_mot_h_muet(mot_suiv):
            # liaison entre un mot a priori terminé par un 's' et un mot commenant par un h muet
            return True    

    return False


######################################################################################
#
######################################################################################
if __name__ == "__main__":


    lcdict = LCDictionnary('lirecouleur.dic')

    if len(sys.argv) == 3:
        print ('test de liaison')
        print (pretraitement_texte(u(sys.argv[1])) + ' ' + pretraitement_texte(u(sys.argv[2])) + ' : ' + str(teste_liaison(pretraitement_texte(u(sys.argv[1])), pretraitement_texte(u(sys.argv[2])))))
    else:
        l_mots = []
        for i in range(len(sys.argv) - 1):
            l_mots.append(u(sys.argv[i + 1]))
        if len(l_mots) == 0:
            l_mots = ['lirecouleur', 'éléphant']
        
        for mot in l_mots:
            message_test = mot
            print ('test chaine de phonemes debutant lecteur : ' + message_test)
            pp = generer_paragraphe_phonemes(message_test, 1)
            print (message_test + ': ' + str(pp))
            print ('\n')
            print ('test chaine de phonemes : ' + message_test)
            pp = generer_paragraphe_phonemes(message_test)
            print (message_test + ': ' + str(pp))
            print ('\n')
            print ('test chaine de syllabes : ' + message_test)
            ps = generer_paragraphe_syllabes(pp)
            print (message_test + ': ' + str(ps))
            print ('\n')
            print ('test chaine de syllabes orales : ' + message_test)
            ps = generer_paragraphe_syllabes(pp, (ConstLireCouleur.SYLLABES_LC, ConstLireCouleur.SYLLABES_ORALES))
            print (message_test + ': ' + str(ps))
            print ('\n')
            print ('test chaine de syllabes ecrites : ' + message_test)
            ps = generer_paragraphe_syllabes(pp, (ConstLireCouleur.SYLLABES_STD, ConstLireCouleur.SYLLABES_ECRITES))
            print (message_test + ': ' + str(ps))
            print ('\n')
            print ('------------------------------')
